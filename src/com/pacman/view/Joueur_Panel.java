package com.pacman.view;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.pacman.controller.KeyMap_Controller;
import com.pacman.model.Joueur;

public class Joueur_Panel extends Abs_Panel {
	private final static float  RATIOH = (float) 2/3;
	private final static float  RATIOL = (float) 1/2;
	private final static float  POS_X = (float) 0;
	private final static float  POS_Y = (float) 1/6;

	private final static String TXT_NAME = "Joueur : ";
	private final static String TXT_SCORE = "Score : ";
	private final static String TXT_LIVE = "Vies : ";
	private final static String TXT_BONUS = "Bonus : ";
	
	public static final String H = "^",B = "v", G = "<", D = ">";

	Joueur j;

	JLabel name, scores, lives, bonus;
	JPanel infos;
	JPanel keyMap;


	public Joueur_Panel(Abs_Panel _mother,Joueur _j) {
		super(_mother);
		this.j = _j;
		this.setLayout(new GridLayout(2,1));

		name = new JLabel(TXT_NAME+j.getName());

		scores = new JLabel(TXT_SCORE+j.getScore()+"");

		lives = new JLabel(TXT_LIVE+j.getLives()+"");

		bonus = new JLabel(TXT_BONUS+j.getBonus()+"");

		infos = new JPanel();
		infos.setLayout(new GridLayout(4,1));
		
		infos.add(name);
		infos.add(scores);
		infos.add(lives);
		infos.add(bonus);
		this.add(infos);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		name.setText(TXT_NAME+j.getName());
		scores.setText(TXT_SCORE+j.getScore()+"");
		lives.setText(TXT_LIVE+j.getLives()+"");
		bonus.setText(TXT_BONUS+j.getBonus()+"");
	}

	@Override
	public float getRatioH() {
		return this.RATIOH;
	}


	@Override
	public float getRatioL() {
		return this.RATIOL;
	}


	@Override
	public float getPOS_X() {
		return j.getNum()==1?this.POS_X:(float)(3.0/4.0);
	}


	@Override
	public float getPOS_Y() {
		return this.POS_Y;
	}

	@Override
	public void refreshInfo() {
		repaint();
	}

	public void buildKeyMap() {
		keyMap = new JPanel();
		keyMap.setLayout(new GridLayout(3,1));
		JButton h,b,g,d;
		h = new JButton(H);
		b = new JButton(B);
		g = new JButton(G);
		d = new JButton(D);
		
		h.setName(h.getText());
		b.setName(b.getText());
		g.setName(g.getText());
		d.setName(d.getText());
		
		h.addActionListener(new KeyMap_Controller(this,h.getName()));
		b.addActionListener(new KeyMap_Controller(this,b.getName()));
		g.addActionListener(new KeyMap_Controller(this,g.getName()));
		d.addActionListener(new KeyMap_Controller(this,d.getName()));
		
		keyMap.add(h);
		keyMap.add(g);
		keyMap.add(d);
		keyMap.add(b);
		this.add(keyMap);
	}

}
