package com.pacman.view;

import javax.swing.JPanel;

import com.pacman.tools.Dimension;

public class Abs_Panel extends JPanel {
	Abs_Fenetre motherF;
	Abs_Panel motherP;
	Dimension dim;

	public Abs_Panel(Abs_Panel _mother) {

		motherP = _mother;
	}

	public Abs_Panel(Abs_Fenetre _mother) {
		motherF = _mother;
	}

	public Abs_Fenetre getMotherF() {
		return motherF;
	}

	public void setMotherF(Abs_Fenetre _mother) {
		this.motherF = _mother;
	}

	public Abs_Panel getMotherP() {
		return motherP;
	}

	public void setMotherP(Abs_Panel _mother) {
		this.motherP = _mother;
	}

	public Dimension getDim() {
		return dim;
	}

	public void setDim(Dimension dim) {
		this.dim = dim;
	}

	public void changerVue(Abs_Panel next) {
		motherF.changerVue(next);
	}
	public void pause() throws InterruptedException{
	}
	public void resume(){
	}
	public void options(){	
	}

	public void refreshInfo() {
	}
	public float getRatioH(){
		return (float) 0.0;
	}
	public float getRatioL(){
		return (float) 0.0;
	}
	public float getPOS_X(){
		return (float) 0.0;
	}
	public float getPOS_Y(){
		return (float) 0.0;
	}
	public int getType(){
		return 1;
	}
}
