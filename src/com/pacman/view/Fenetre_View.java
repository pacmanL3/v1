package com.pacman.view;

import java.awt.Container;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.pacman.controller.KeyListener_Controller;
import com.pacman.controller.MouseMotionListener_controller;
import com.pacman.controller.Windows_resize_listener_Controller;
import com.pacman.model.Jeu;
import com.pacman.model.Niveau;
import com.pacman.tools.Dimension;

/**
 * 
 * @author yrichi
 *
 *
 *         Corespond à la frame de base c'est ici qu'on va ajouter les classes
 *         etendant de Jpanel
 */
public class Fenetre_View extends Abs_Fenetre {
	private boolean accueil_visible = true;
	private boolean test_visible = false;
	private int hauteur = 400;
	private int largeur = 700;


	public Fenetre_View() {
		super();
		this.setTitle("titre : Pacman");
		this.dim = new Dimension(largeur, hauteur);

		this.setSize(this.dim.getX(), this.dim.getY());

		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		this.addMouseMotionListener(new MouseMotionListener_controller(this));
		this.setFocusable(true);
		this.addKeyListener(new KeyListener_Controller(this));
		this.addComponentListener(new Windows_resize_listener_Controller(this));

		current_panel = new Accueil_View(this);
		this.add(current_panel);

		this.setVisible(true);
	}
	
	public void newGame(String mode){
		set_Last_key_pressed("",1);
		set_Last_key_pressed("",2);
		game = new Jeu(this,mode);
	}
	
	public void gameOver(){
		super.gameOver();
		changerVue(new GameOver_Panel(this,game));
	}
	
	@Override
	public void pause() {
		getTimer_game().setPause(true);
	}
	
	@Override
	public void resume() {
		getTimer_game().setPause(false);
	}

	
	@Override
	public void alert_pos_changed(int x, int y) {
		super.alert_pos_changed(x,y);
		if(dev!=null)
			dev.refreshInfo();
	}
	
	@Override
	public void refreshInfo() {
		super.refreshInfo();
		current_panel.refreshInfo();
		if(dev != null)
			dev.refreshInfo();
	}
	
	@Override
	public void refreshActions() {
		super.refreshActions();
		refreshInfo();
		if(jeu != null && jeu.center != null){
			((Center_Panel)(jeu.center)).refresh_move_game();
		}
	}
	
	@Override
	public void refreshInfoJeu() {
		super.refreshInfoJeu();
	}

}
