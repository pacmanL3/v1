package com.pacman.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.pacman.controller.Head_Controler;

public class Head_Panel extends Abs_Panel {
	private final static float  RATIOH = (float) 1/6;
	private final static float  RATIOL = (float) 1;
	private final static float  POS_X = (float) 0;
	private final static float  POS_Y = (float) 0;
	public final static String HOME = "home";
	public final static String PAUSE = "pause";
	JButton home, pause;
	
	public Head_Panel(Jeux_Panel jeux_View) {
		super(jeux_View);
		home = new JButton(HOME);
		pause = new JButton(PAUSE);
		home.setName(home.getText());
		pause.setName(pause.getText());
		home.addActionListener(new Head_Controler(this,home.getText()));
		pause.addActionListener(new Head_Controler(this,pause.getText()));
		this.setLayout(new GridLayout(1,3));
		this.add(home);
		this.add(new JLabel("PACMAN"));
		this.add(pause);
	}
	
	@Override
	public float getRatioH() {
		return this.RATIOH;
	}


	@Override
	public float getRatioL() {
		return this.RATIOL;
	}


	@Override
	public float getPOS_X() {
		return this.POS_X;
	}


	@Override
	public float getPOS_Y() {
		return this.POS_Y;
	}

}
