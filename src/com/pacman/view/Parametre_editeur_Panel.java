package com.pacman.view;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.pacman.controller.Parametre_editor_Controller;
import com.pacman.model.Editeur_map;

import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FlowLayout;
import javax.swing.JComboBox;

public class Parametre_editeur_Panel extends Abs_Panel {

	public static final String MOINSL = "MOINSL";
	public static final String PLUSL = "PLUSL";
	public static final String MOINSC = "MOINSC";
	public static final String PLUSC = "PLUSC";
	public static final String VALIDER = "VALIDER";
	public static final String RETOUR = "RETOUR";
	public static final String CHARGER = "charger";

	JButton bt_moinsl;
	JTextField text_ligne;
	JButton bt_plusl;
	JButton bt_moinsc;
	JTextField text_colonne;
	JButton bt_plusc;
	JLabel label_nbrligne;
	JLabel label_nbrcolonne;

	JButton bt_retour;
	JButton bt_valider;
	private JPanel haut;
	private JLabel label;
	private JPanel panel_1;
	private JPanel bas;
	private JPanel panel_3;
	private JPanel panel_4;
	private final JPanel centre = new JPanel();
	private JComboBox<String> comboBox;
	private JLabel lblChargerUnFichier;
	private JPanel centre_chargement_fichier;
	private JPanel panel;
	private JButton btnCharger;

	private String[] liste_fichier_chargement;



	public Parametre_editeur_Panel(Abs_Panel _mother) {
		super(_mother.getMotherF());
		setLayout(new GridLayout(0, 1, 0, 0));

		haut = new JPanel();
		add(haut);

		panel_3 = new JPanel();
		haut.add(panel_3);

		label_nbrcolonne = new JLabel("Nombre de Colonne");
		panel_3.add(label_nbrcolonne);

		panel_1 = new JPanel();
		haut.add(panel_1);
		bt_moinsl = new JButton("-");
		haut.add(bt_moinsl);
		bt_moinsl.setName(MOINSL);
		text_ligne = new JTextField("10", 5);
		haut.add(text_ligne);
		bt_plusl = new JButton("+");
		haut.add(bt_plusl);
		bt_plusl.setName(PLUSL);
		label_nbrligne = new JLabel("Nombre de ligne");
		haut.add(label_nbrligne);
		bt_moinsc = new JButton("-");
		haut.add(bt_moinsc);

		bt_moinsc.setName(MOINSC);
		text_colonne = new JTextField("10", 5);
		haut.add(text_colonne);
		bt_plusc = new JButton("+");
		haut.add(bt_plusc);
		bt_plusc.setName(PLUSC);
		
		panel_4 = new JPanel();
		haut.add(panel_4);
		bt_plusc.addActionListener(new Parametre_editor_Controller(PLUSC, this));

		bt_moinsc.addActionListener(new Parametre_editor_Controller(MOINSC, this));
		bt_plusl.addActionListener(new Parametre_editor_Controller(PLUSL, this));
		bt_moinsl.addActionListener(new Parametre_editor_Controller(MOINSL, this));
		add(centre);
		
		centre_chargement_fichier = new JPanel();
		centre.add(centre_chargement_fichier);
		
		lblChargerUnFichier = new JLabel("Charger un fichier de sauvegarde");
		centre_chargement_fichier.add(lblChargerUnFichier);
		
		liste_fichier_chargement= getListe_fichier_chargement();
		
		comboBox = new JComboBox();
		

		for (int i = 0; i < liste_fichier_chargement.length; i++) {
			comboBox.addItem(liste_fichier_chargement[i]);
		}
		
		comboBox.addActionListener((new Parametre_editor_Controller("Jcombobox",this)));
		centre_chargement_fichier.add(comboBox);
		
		
		panel = new JPanel();
		centre.add(panel);
		
		btnCharger = new JButton("Charger");
		btnCharger.setName(CHARGER);
		btnCharger.addActionListener(new Parametre_editor_Controller(CHARGER,this));
		btnCharger.setEnabled(false);
		panel.add(btnCharger);

		bas = new JPanel();
		add(bas);
		bas.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		bt_valider = new JButton("OK");
		bas.add(bt_valider);
		bt_valider.setName(VALIDER);
		bt_valider.addActionListener(new Parametre_editor_Controller(VALIDER, this));

		bt_retour = new JButton("Retour");
		bt_retour.setName(RETOUR);
		bas.add(bt_retour);
		bt_retour.addActionListener(new Parametre_editor_Controller(RETOUR, this));

	}

	public int getValeurligne() {
		String ligne = text_ligne.getText();
		return Integer.parseInt(ligne);

	}

	public int getValeurColonne() {
		String colonne = text_colonne.getText();
		System.out.println("traceur paramettre " + Integer.parseInt(colonne));
		return Integer.parseInt(colonne);

	}

	public void setValeurligne(int i) {
		text_ligne.setText(String.valueOf(i));
	}

	public void setValeurColonne(int i) {
		text_colonne.setText(String.valueOf(i));
	}

	public String[] getListe_fichier_chargement() {
		liste_fichier_chargement = Editeur_map.get_all_fichier();
		return liste_fichier_chargement;
	}

	public void setListe_fichier_chargement(String[] liste_fichier_chargement) {
		this.liste_fichier_chargement = liste_fichier_chargement;
	}

	public JButton getBtnCharger() {
		return btnCharger;
	}

	public JComboBox<String> getComboBox() {
		return comboBox;
	}
	
}
