package com.pacman.view;

import com.pacman.controller.*;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TestView extends Abs_Panel {
	Fenetre_View mother;
	JLabel test_01;
	JButton bt_test;
	String st_test;
	final static String BT1 = "bouton_1";

	public TestView(Fenetre_View _mother) {
		super(_mother);
		// TODO Auto-generated constructor stub
		mother = _mother;
		test_01 = new JLabel("defaut");
		this.add(test_01);

		bt_test = new JButton(BT1);
		bt_test.setName(bt_test.getText());
		bt_test.addActionListener(new Test_controller(this, bt_test.getName()));

		this.add(bt_test);

	}

	public void set_test(String s) {
		st_test = s;
		test_01.setText(s);
	}

}
