package com.pacman.view;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JLabel;

import com.pacman.model.Joueur;

public class Foot_Panel extends Abs_Panel {
	private final static float  RATIOH = (float) 1/6;
	private final static float  RATIOL = (float) 1;
	private final static float  POS_X = (float) 0;
	private final static float  POS_Y = (float) 5/6;
	
	private final static String TXT_NAME = "Joueur : ";
	private final static String TXT_SCORE = "Score : ";
	private final static String TXT_LIVE = "Vies : ";
	private final static String TXT_BONUS = "Bonus : ";
	
	Joueur j1, j2;
	
	JLabel scores, bonus;
	
	public Foot_Panel(Abs_Panel _mother, Joueur _j1, Joueur _j2) {
		
		super(_mother);
		
		this.j1 = _j1;
		this.j2 = _j2;
		this.setLayout(new GridLayout(1,2));
		
		scores = new JLabel();
		scores.setText(TXT_SCORE+(j1.getScore()+j2.getScore()));
		
		bonus = new JLabel();
		bonus.setText(TXT_BONUS+(j1.getBonus()+j2.getBonus()));
		
		this.add(scores);
		this.add(bonus);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		scores.setText(TXT_SCORE+(j1.getScore()+j2.getScore()));
		bonus.setText(TXT_BONUS+(j1.getBonus()+j2.getBonus()));
	}


	@Override
	public float getRatioH() {
		// TODO Auto-generated method stub
		return this.RATIOH;
	}


	@Override
	public float getRatioL() {
		// TODO Auto-generated method stub
		return this.RATIOL;
	}


	@Override
	public float getPOS_X() {
		// TODO Auto-generated method stub
		return this.POS_X;
	}


	@Override
	public float getPOS_Y() {
		// TODO Auto-generated method stub
		return this.POS_Y;
	}

	@Override
	public void refreshInfo() {
		repaint();
	}
}
