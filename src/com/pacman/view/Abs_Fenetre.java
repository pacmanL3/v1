package com.pacman.view;

import com.pacman.controller.MouseMotionListener_controller;
import com.pacman.model.Carte;
import com.pacman.model.Editeur_map;
import com.pacman.model.Jeu;
import com.pacman.model.Niveau;
import com.pacman.model.PacMan;
import com.pacman.tools.*;
import javax.swing.JFrame;

public abstract class Abs_Fenetre extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Abs_Fenetre mother;
	protected Dimension dim;
	protected Dimension position_souris;
	protected String type_fenetre;
	protected Jeux_Panel jeu ;
	protected Abs_Panel current_panel;
	protected Jeu game;
	protected Editeur_map editeurJeu ; 
	protected Editeur_Panel editpanel;
	protected Fenetre_Panneau_info_dev_View dev;
	protected String last_key_pressed_1,last_key_pressed_2;
	protected GameTimer timer_game;
	protected String controllsA;
	protected String controllsB;



	public static final String HAUT = "haut";
	public static final String BAS = "bas";
	public static final String GAUCHE = "gauche";
	public static final String DROITE = "droite";


	public Editeur_map getEditeurJeu() {
		return editeurJeu;
	}

	public void New_editeur(int i ,int j){
		editeurJeu = new Editeur_map(i, j);
	}

	public void New_editeur(String s,Carte c, int i , int j){
		editeurJeu = new Editeur_map(s,c,i, j);
	}


	public void newGame(String string){
	}
	public Jeu getGame() {
		return game;
	}

	public void newTimer(){
		if(getTimer_game() == null){
			setTimer_game(new GameTimer(this));
			getTimer_game().start();
		}
	}

	public GameTimer getTimer(){
		return getTimer_game();
	}


	protected Abs_Fenetre(Abs_Fenetre _mother) {
		this.mother = _mother;
	}
	protected Abs_Fenetre(){
		this.position_souris = new Dimension(0, 0);
	}
	public Dimension getDim(){
		return dim;
	}
	public void setDim(Dimension dim){
		this.dim = dim;
	}
	public Abs_Fenetre getMother(){
		return mother;
	}
	public void setMother(Abs_Fenetre mother){
		this.mother = mother;
	}
	public String getType_fenetre(){
		return type_fenetre;
	}
	public void setType_fenetre(String type_fenetre){
		this.type_fenetre = type_fenetre;
	}
	public void set_pos_x(int _newX) {
		// TODO Auto-generated method stub
		this.position_souris.setX(_newX);
	}
	public void set_pos_y(int _newY) {
		// TODO Auto-generated method stub
		this.position_souris.setY(_newY);
	}
	public void set_Last_key_pressed(String k, int joueur){

		if(joueur == 1){
			if(k != last_key_pressed_1){
				last_key_pressed_1 = k;
			}
		}else if(joueur == 2){
			if(k != last_key_pressed_2){
				last_key_pressed_2 = k;
			}
		}
	}
	public String get_Last_key_pressed(int num){
		return num == 1 ? this.last_key_pressed_1 : last_key_pressed_2;
	}
	public void refreshInfo(){
		this.setDim(new Dimension(this.getBounds()));
	}
	public void options(){
		if(dev!=null){
			dev.setVisible(!dev.isVisible());	
		}else{
			dev = new Fenetre_Panneau_info_dev_View(this);
		}
	}
	public void changerVue(Abs_Panel next){
		this.current_panel = next;
		this.getContentPane().removeAll();
		this.add(next);
		this.setVisible(true);
		this.revalidate();
		this.repaint();
	}



	public void alert_pos_changed(int x, int y) {
		this.set_pos_x(x);
		this.set_pos_y(y);
	}

	public Abs_Panel getCurrentPanel(){
		return this.current_panel;
	}
	public void refreshActions() {	
	}
	public void refreshInfoJeu(){
		if(game != null && current_panel != null){
			current_panel.refreshInfo();
		}
	}
	public void pause(){

	}
	public void resume(){

	}

	public void gameOver(){
		getTimer_game().setPause(true);
		getTimer_game().cancel();
		//timer_game = null;
	}

	public GameTimer getTimer_game() {
		return timer_game;
	}

	public void setTimer_game(GameTimer timer_game) {
		this.timer_game = timer_game;
	}

	public void setControlls(String _controllsA, String _controllsB) {
		this.controllsA = _controllsA;
		this.controllsB = _controllsB;
	}

	public String getControllsA() {
		return controllsA;
	}

	public String getControllsB() {
		return controllsB;
	}

	public int getNumJoueurControls(String controlType) {
		if(controlType.equals(getControllsA())){
			return 1;
		}else if(controlType.equals(getControllsB())){
			return 2;
		}else return 0;
	}
}
