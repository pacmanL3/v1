package com.pacman.view;

import com.pacman.controller.Editeur_Controleur;
import com.pacman.model.Blinky;
import com.pacman.model.Carte;
import com.pacman.model.Case;
import com.pacman.model.Mur;
import com.pacman.model.PacMan;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

public class Editeur_Panel extends Abs_Panel {
	JPanel jeu,option;
	private JButton valider_bt;
	public static final  String VALIDER_LABEL = "VALIDER";

	private JButton retour_bt;
	public static final String RETOUR_LABEL = "RETOUR";
	
	int largeur; // d'une case
	int hauteur;
	int pos_x; // coordonne de la souris
	int pos_y;
	PacMan p1,p2;
	Blinky fantomes;

	public Editeur_Panel(Abs_Panel _mother) {
		super(_mother);
		setLayout(new BorderLayout(0, 0)); 
		 jeu = new JPanel();
		add(jeu, BorderLayout.CENTER);
		this.addMouseListener(new Editeur_Controleur(this));
		this.addMouseWheelListener(new Editeur_Controleur(this));
		this.addMouseMotionListener(new Editeur_Controleur(this));
		 option = new JPanel();
		valider_bt = new JButton("valider");
		option.add(valider_bt);
		valider_bt.setText(VALIDER_LABEL);
		valider_bt.addActionListener(new Editeur_Controleur(this, VALIDER_LABEL));
		
		retour_bt = new JButton("retour");
		option.add(retour_bt);
		retour_bt.setText(RETOUR_LABEL);
		retour_bt.addActionListener(new Editeur_Controleur(this , RETOUR_LABEL));
		
		add(option, BorderLayout.SOUTH);
		jeu.setBorder(BorderFactory.createLineBorder(Color.black));
		
		
		p1 = new PacMan(0, 0, Case.PACMAN);
		p2 = new PacMan(0, 0, Case.PACMAN2);
		fantomes = new Blinky(0, 0);
	}
	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		Abs_Fenetre m = this.getMotherP().getMotherF();
		Carte tmp = getMotherP().getMotherF().getEditeurJeu().getMap_game();
		//System.out.println(getSize().toString());
		hauteur = (int)(jeu.getSize().getHeight()/tmp.getNb_lig());
		for (int j = 0; j < tmp.getNb_lig(); j++) {
			largeur = (int)(jeu.getSize().getWidth()/tmp.getNb_col());
			for (int i = 0; i < tmp.getNb_col(); i++) {
				Case c = tmp.get(i, j);
				Image img = c.getImage();
				//System.out.println("traceur editeur panel img : "+img);
				g2d.drawImage(img,largeur * j, hauteur * i,largeur,hauteur, null);
			}
		}
		if (tmp.GetPosinitialPacman1() != null){
			p1.setPos(tmp.GetPosinitialPacman1());
			g2d.drawImage(p1.getImage(), largeur * p1.getPos().getX(), hauteur *  p1.getPos().getY(), largeur, hauteur, null);
		}
		
		if (tmp.GetPosinitialPacman2() != null){
			p2.setPos(tmp.GetPosinitialPacman2());
			g2d.drawImage(p2.getImage(), largeur * p2.getPos().getX(), hauteur *  p2.getPos().getY(), largeur, hauteur, null);
		}
		
		if (tmp.GetPosinitialfantomes() != null){
			fantomes.setPos(tmp.GetPosinitialfantomes());
			g2d.drawImage(fantomes.getImage(), largeur * fantomes.getPos().getX(), hauteur *  fantomes.getPos().getY(), largeur, hauteur, null);
		}
		
		
		afficheSelectionCourante(g);
	
	}
	
	
	public void afficheSelectionCourante(Graphics g ){
		Graphics2D g2d = (Graphics2D) g;
		if (getMotherP().getMotherF().getEditeurJeu().getTmp()!= null) {
			Case c = getMotherP().getMotherF().getEditeurJeu().getTmp();
			Image img = c.getImage();
			g2d.drawImage(img,pos_x, pos_y,largeur,hauteur, null);
//			System.out.println("hauteur "+hauteur +"largeur "+largeur);
//			System.out.println("pael jeu hauteur : "+jeu.getWidth()+"  larg "+jeu.getSize().getHeight());
				
		}
		
	}

	

	public int getPos_x() {
		return pos_x;
	}


	public void setPos_x(int pos_x) {
		this.pos_x = pos_x;
	}


	public int getPos_y() {
		return pos_y;
	}


	public void setPos_y(int pos_y) {
		this.pos_y = pos_y;
	}
	
	public JPanel getJeu_Editeur_panel(){
		return jeu;
	}

	
	public int Hauteur_Panel_jeu(){
		return jeu.getHeight();
	}

	
	public int Hauteur_Panel_Option(){
		return option.getHeight();
	}
	
	public int Largeur_Panel_jeu(){
		return jeu.getWidth();
		
	}

}