package com.pacman.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.pacman.controller.Victory_Controller;
import com.pacman.model.Jeu;

public class Victory_Panel extends Abs_Panel {

	public static final String HOME = "Home";
	public static final String CONTINUER = "Niveau suivant";
	
	private JPanel titre;
	private JPanel recap;
	private JPanel options;
	
	private Jeu jeu;
	
	public Victory_Panel(Fenetre_View _mother, Jeu _jeu){
		super(_mother);
		jeu = _jeu;
		build();
	}
	
	private void build(){
		this.setLayout(new BorderLayout());
		
		titre = new JPanel();
		recap = new JPanel();
		options = new JPanel();
		
		titre.setLayout(new BorderLayout());
		titre.add(new JLabel("Victoire !!! Niveau "+jeu.getCurrentLevel()+"/"+jeu.getMaxLevel()),BorderLayout.CENTER);
		
		recap.setLayout(new BorderLayout());
		if(jeu.getMode().equals(Choix_Jeu_Panel.SOLO)){
			recap.add(new Joueur_Panel(this, jeu.getJoueur(1)),BorderLayout.CENTER);
		}else{
			recap.add(new Joueur_Panel(this, jeu.getJoueur(1)),BorderLayout.WEST);
			recap.add(new Joueur_Panel(this, jeu.getJoueur(2)),BorderLayout.EAST);
		}
		
		options.setLayout(new GridLayout(1,2));
		
		JButton bt_continuer = new JButton();
		JButton bt_home = new JButton();
		
		bt_continuer.setName(CONTINUER);
		bt_home.setName(HOME);
		
		bt_continuer.setText(bt_continuer.getName());
		bt_home.setText(bt_home.getName());
		
		bt_continuer.addActionListener(new Victory_Controller(this,bt_continuer.getName()));
		bt_home.addActionListener(new Victory_Controller(this,bt_home.getName()));
		
		bt_continuer.setEnabled(jeu.getNextLevel() != -1);
		
		options.add(bt_continuer);
		options.add(bt_home);
		
		this.add(titre,BorderLayout.NORTH);
		this.add(recap,BorderLayout.CENTER);
		this.add(options,BorderLayout.SOUTH);
		
		
	}
}
