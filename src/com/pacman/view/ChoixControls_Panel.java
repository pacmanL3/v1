package com.pacman.view;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.pacman.controller.ChoixControls_Controller;

public class ChoixControls_Panel extends Abs_Panel {

	public static final String CLICK_TO_GO = "suit le clic";
	public static final String MOVE_TO_GO = "suit le pointeur";
	public static final String KEY_MAP = "clavier virtuel";
	public static final String KEYBOARD = "touches fléchées du clavier";
	
	public static final String OK = "Jouer";
	public static final String RETOUR = "Retour";

	public String carte;
	public String mode;
	private JPanel j1,j2;
	
	JRadioButton rbA_click, rbA_move, rbA_key, rbA_kb;
	JRadioButton rbB_click, rbB_move, rbB_key, rbB_kb;
	
	ButtonGroup groupA, groupB;
	
	public ChoixControls_Panel(Abs_Fenetre _mother, String _mode, String _carte){
		super(_mother);
		this.carte = _carte;
		this.mode = _mode;
		build();
	}

	private void build(){
		this.setLayout(new BorderLayout());

		j1 = new JPanel();
		j1.setLayout(new GridLayout(4,1));

		rbA_click = new JRadioButton(CLICK_TO_GO);
		rbA_move = new JRadioButton(MOVE_TO_GO);
		rbA_key = new JRadioButton(KEY_MAP);
		rbA_kb = new JRadioButton(KEYBOARD);
		
		rbA_click.setActionCommand(rbA_click.getText());
		rbA_move.setActionCommand(rbA_move.getText());
		rbA_key.setActionCommand(rbA_key.getText());
		rbA_kb.setActionCommand(rbA_kb.getText());
		
		groupA = new ButtonGroup();
		groupA.add(rbA_click);
		groupA.add(rbA_move);
		groupA.add(rbA_key);
		groupA.add(rbA_kb);
		
		j1.add(rbA_click);
		j1.add(rbA_move);
		j1.add(rbA_key);
		j1.add(rbA_kb);
		this.add(j1,BorderLayout.WEST);
		if(!mode.equals(Choix_Jeu_Panel.SOLO)){
			j2 = new JPanel();
			j2.setLayout(new GridLayout(4,1));

			rbB_click = new JRadioButton(CLICK_TO_GO);
			rbB_move = new JRadioButton(MOVE_TO_GO);
			rbB_key = new JRadioButton(KEY_MAP);
			rbB_kb = new JRadioButton(KEYBOARD);
			
			rbB_click.setActionCommand(rbB_click.getText());
			rbB_move.setActionCommand(rbB_move.getText());
			rbB_key.setActionCommand(rbB_key.getText());
			rbB_kb.setActionCommand(rbB_kb.getText());
			
			groupB = new ButtonGroup();
			groupB.add(rbB_click);
			groupB.add(rbB_move);
			groupB.add(rbB_key);
			groupB.add(rbB_kb);
			
			j2.add(rbB_click);
			j2.add(rbB_move);
			j2.add(rbB_key);
			j2.add(rbB_kb);
			this.add(j2,BorderLayout.EAST);
		}
		
		JButton bt_ok = new JButton();
		JButton bt_retour = new JButton();
		
		bt_ok.setText(OK);
		bt_retour.setText(RETOUR);
		
		bt_ok.setName(bt_ok.getText());
		bt_retour.setName(bt_retour.getText());
		
		bt_ok.addActionListener(new ChoixControls_Controller(this,bt_ok.getName()));
		bt_retour.addActionListener(new ChoixControls_Controller(this,bt_retour.getName()));
		JPanel suite = new JPanel();
		suite.setLayout(new GridLayout(1,2));
		suite.add(bt_ok);
		suite.add(bt_retour);
		this.add(suite,BorderLayout.SOUTH);
	}
	
	public String getSelectionA(){
		return groupA.getSelection() != null ? groupA.getSelection().getActionCommand() : null;
	}
	public String getSelectionB(){
		return groupB != null ? groupB.getSelection().getActionCommand() : null;
	}

	public String getChoixCarte() {
		return this.carte;
	}
}
