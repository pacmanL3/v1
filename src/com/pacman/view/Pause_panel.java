package com.pacman.view;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.pacman.controller.Head_Controler;
import com.pacman.controller.Pause_Controller;

public class Pause_panel extends Abs_Panel {
	private final static float  RATIOH = (float) 1/2;
	private final static float  RATIOL = (float) 1/2;
	private final static float  POS_X = (float) 1/4;
	private final static float  POS_Y = (float) 1/4;
	public final static String HOME = "home";
	public final static String RESUME = "resume";
	public final static String OPTIONS = "info ON/OFF";
	
	private JButton home,resume,options;
	
	public Pause_panel(Abs_Panel jeux_View, Abs_Fenetre _mother) {
		super(jeux_View);
		setMotherF(_mother);
		home = new JButton(HOME);
		resume = new JButton(RESUME);
		options = new JButton(OPTIONS);
		
		
		home.setName(home.getText());
		resume.setName(resume.getText());
		options.setName(options.getText());
		
		home.addActionListener(new Pause_Controller(this,home.getText()));
		resume.addActionListener(new Pause_Controller(this,resume.getText()));
		options.addActionListener(new Pause_Controller(this,options.getText()));
		
		
		this.setLayout(new GridLayout(3,1));
		
		this.add(home);
		this.add(options);
		this.add(resume);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		this.add(home);
		this.add(options);
		this.add(resume);
	}
	
	
	@Override
	public float getRatioH() {
		return this.RATIOH;
	}


	@Override
	public float getRatioL() {
		return this.RATIOL;
	}


	@Override
	public float getPOS_X() {
		return this.POS_X;
	}


	@Override
	public float getPOS_Y() {
		return this.POS_Y;
	}
}
