package com.pacman.view;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Panneau_info_dev_View extends Abs_Panel {
	public JLabel info_size_x, info_size_y, info_pos_x, info_pos_y;
	Abs_Fenetre mother;

	public Panneau_info_dev_View(Abs_Fenetre _mother) {
		super(_mother);
		this.mother = mother;

		info_size_x = new JLabel();
		info_size_y = new JLabel();
		info_pos_x = new JLabel();
		info_pos_y = new JLabel();

		this.setLayout(new GridLayout(4, 2));
		this.add(new JLabel("info_size_x : "));
		this.add(info_size_x);
		this.add(new JLabel("info_size_y : "));
		this.add(info_size_y);
		this.add(new JLabel("info_pos_x : "));
		this.add(info_pos_x);
		this.add(new JLabel("info_pos_y : "));
		this.add(info_pos_y);

	}

	public void refresh() {
		setInfo_size_x();
		setInfo_size_y();
		setInfo_pos_x();
		setInfo_pos_y();
	}

	public void setInfo_size_x() {
		this.info_size_x.setText(getMotherF().getMother().dim.getX() + "");
	}

	public void setInfo_size_y() {
		this.info_size_y.setText(getMotherF().getMother().dim.getY() + "");
	}

	public void setInfo_pos_x() {
		this.info_pos_x.setText(getMotherF().getMother().position_souris.getX() + "");
	}

	public void setInfo_pos_y() {
		this.info_pos_y.setText(getMotherF().getMother().position_souris.getY() + "");
	}

}
