package com.pacman.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.BorderFactory;

import com.pacman.model.Blinky;
import com.pacman.model.Case;
import com.pacman.model.Clyde;
import com.pacman.model.Couloir;
import com.pacman.model.Fantome;
import com.pacman.model.Inky;
import com.pacman.model.Jeu;
import com.pacman.model.PacMan;
import com.pacman.model.Pinky;
import com.pacman.tools.Dimension;

public class Center_Panel extends Abs_Panel {
	private final static float  RATIOH = (float) 2/3;
	private final static float  RATIOL = (float) 1/2;
	private final static float  POS_X = (float) 1/4;
	private final static float  POS_Y = (float) 1/6;
	public  int largeur; // d'une case
	public  int hauteur;

	private Jeu jeu;
	private Dimension caseToGo1;
	private Dimension caseToGo2;

	public Center_Panel(Abs_Panel _mother) {
		super(_mother);
		jeu = getMotherP().getMotherF().getGame();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		Abs_Fenetre m = this.getMotherP().getMotherF();


		refresh_move_game();

		largeur = (int)(getSize().getWidth()/jeu.getMap().getNb_col());
		for (int j = 0; j < jeu.getMap().getNb_col(); j++) {
			hauteur = (int)( getSize().getHeight()/jeu.getMap().getNb_lig());
			for (int i = 0; i < jeu.getMap().getNb_lig(); i++) {
				Case c = jeu.getMap().get(j,i);
				Image img = c.getImage();
				g2d.drawImage(img,largeur * j, hauteur * i,largeur,hauteur, null);
				if(c.getType().equals(Case.COULOIR)){
					if(((Couloir)c).getBonus()!=null){
						g2d.drawImage(((Couloir)c).getBonus().getImage(),largeur * j, hauteur * i,largeur/2,hauteur/2, null);
					}
				}
			}
		}
		afficher_Pacman(g2d,jeu.getMap().getNb_col(),jeu.getMap().getNb_lig());
		afficher_Fantomes(g2d,jeu.getMap().getNb_col(),jeu.getMap().getNb_lig());
	}


	public void afficher_Pacman(Graphics2D g, int nbl, int nbc){
		PacMan pacman = jeu.getPacman();  
		Image img;
		if(pacman != null){
			img = pacman.getImage();
			g.drawImage(img,pacman.getPos().getX(), pacman.getPos().getY(),(int)(getSize().getWidth()/nbc),(int)(getSize().getHeight()/nbl), null);
		}
		if(!jeu.getMode().equals(Choix_Jeu_Panel.SOLO) && jeu.getPacman_bis() != null){
			pacman = jeu.getPacman_bis();  
			img = pacman.getImage();
			g.drawImage(img,pacman.getPos().getX(), pacman.getPos().getY(),(int)(getSize().getWidth()/nbc),(int)(getSize().getHeight()/nbl), null);
		}
	}

	public void afficher_Fantomes(Graphics2D g, int nbl, int nbc){
		Fantome[] fantomes = jeu.getFantomes();
		Image img;
		for(int i = 0; i < fantomes.length; i++){
			switch(fantomes[i].getName()){
			case "Blinky":
				img = ((Blinky)fantomes[i]).getImage();
				break;
			case "Inky":
				img = ((Inky)fantomes[i]).getImage();
				break;
			case "Pinky":
				img = ((Pinky)fantomes[i]).getImage();
				break;
			case "Clyde":
				img = ((Clyde)fantomes[i]).getImage();
				break;
			default : img = null;
			}
			g.drawImage(img,fantomes[i].getPos().getX(), fantomes[i].getPos().getY(),(int)(getSize().getWidth()/nbc),(int)(getSize().getHeight()/nbl), null);
		}
	}

	public void refresh_move_game(){ // permet de mettre a jour les deplacement du pacman
		PacMan pacman = jeu.getPacman();
		PacMan pacman_bis = jeu.getPacman_bis();
		if(pacman != null){
			if(getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.CLICK_TO_GO)==1){
				pacman.setCaseToGo(caseToGo1);
			}else if(getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.MOVE_TO_GO)==1){
				pacman.setCaseToGo(caseToGo1);
			}else{
				pacman.setNextDir(getMotherP().getMotherF().get_Last_key_pressed(1));
			}
		}
		if(pacman_bis != null){
			if(getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.CLICK_TO_GO)==2){
				pacman_bis.setCaseToGo(caseToGo2);
			}else if(getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.MOVE_TO_GO)==2){
				pacman_bis.setCaseToGo(caseToGo2);
			}else{
				pacman_bis.setNextDir(getMotherP().getMotherF().get_Last_key_pressed(1));
			}
		}
		jeu.avancer();
	}



	@Override
	public float getRatioH() {
		return this.RATIOH;
	}

	@Override
	public float getRatioL() {
		return this.RATIOL;
	}

	@Override
	public float getPOS_X() {
		return this.POS_X;
	}

	@Override
	public float getPOS_Y() {
		return this.POS_Y;
	}

	public void refreshInfo() {
		refresh();
		repaint();
	}

	public void refresh(){
	}

	public Jeu getGame() {
		return jeu;
	}

	public void setCaseToGo(int num, Dimension caseToGo) {
		if(num == 1){
			caseToGo1 = caseToGo;
		}else if(num == 2){
			caseToGo2 = caseToGo;
		}
	}
}

