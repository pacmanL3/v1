package com.pacman.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.pacman.controller.GameOver_Controller;
import com.pacman.controller.Victory_Controller;
import com.pacman.model.Jeu;

public class GameOver_Panel extends Abs_Panel {

	public static final String HOME = "Home";
	public static final String RECOMMENCER = "Recommencer";
	
	private JPanel titre;
	private JPanel recap;
	private JPanel options;
	
	private Jeu jeu;
	
	public GameOver_Panel(Fenetre_View _mother, Jeu _jeu){
		super(_mother);
		jeu = _jeu;
		build();
	}
	
	private void build(){
this.setLayout(new BorderLayout());
		
		titre = new JPanel();
		recap = new JPanel();
		options = new JPanel();
		
		titre.setLayout(new BorderLayout());
		titre.add(new JLabel("Défaite !!! Niveau "+jeu.getCurrentLevel()+"/"+jeu.getMaxLevel()),BorderLayout.CENTER);
		
		recap.setLayout(new BorderLayout());
		if(jeu.getMode().equals(Choix_Jeu_Panel.SOLO)){
			recap.add(new Joueur_Panel(this, jeu.getJoueur(1)),BorderLayout.CENTER);
		}else{
			recap.add(new Joueur_Panel(this, jeu.getJoueur(1)),BorderLayout.EAST);
			recap.add(new Joueur_Panel(this, jeu.getJoueur(2)),BorderLayout.WEST);
		}
		
		options.setLayout(new GridLayout(1,2));
		
		JButton bt_recommencer = new JButton();
		JButton bt_home = new JButton();
		
		bt_home.setName(HOME);
		bt_recommencer.setName(RECOMMENCER);
		
		bt_home.setText(bt_home.getName());
		bt_recommencer.setText(bt_recommencer.getName());
		
		bt_home.addActionListener(new GameOver_Controller(this,bt_home.getName()));
		bt_recommencer.addActionListener(new GameOver_Controller(this,bt_recommencer.getName()));
		
		options.add(bt_home);
		options.add(bt_recommencer);
		
		this.add(titre,BorderLayout.NORTH);
		this.add(recap,BorderLayout.CENTER);
		this.add(options,BorderLayout.SOUTH);
	}
}
