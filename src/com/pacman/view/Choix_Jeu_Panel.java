package com.pacman.view;

import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.pacman.controller.Choix_Jeu_Controller;
import com.pacman.controller.Parametre_editor_Controller;
import com.pacman.model.Editeur_map;

public class Choix_Jeu_Panel extends Abs_Panel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static String SOLO = "solo";
	public final static String VS = "vs";
	public final static String COOP = "coop";


	public final static String RETOUR = "retour";

	private JButton solo, vs, coop,bt_retour;



	private final JPanel centre = new JPanel();
	private JComboBox<String> comboBox;
	private JLabel lblChargerUnFichier;
	private JPanel centre_chargement_fichier;
	private JPanel panel;
	private JButton btnCharger;

	private String[] liste_fichier_chargement;
	
	public Choix_Jeu_Panel(Abs_Fenetre _mother) {
		super(_mother);
		solo = new JButton(SOLO);
		vs = new JButton(VS);
		coop = new JButton(COOP);
		bt_retour = new JButton(RETOUR);


		solo.setName(solo.getText());
		vs.setName(vs.getText());
		coop.setName(coop.getText());
		bt_retour.setName(RETOUR);

		solo.addActionListener(new Choix_Jeu_Controller(this,SOLO));
		vs.addActionListener(new Choix_Jeu_Controller(this,VS));
		coop.addActionListener(new Choix_Jeu_Controller(this, COOP));
		bt_retour.addActionListener(new Choix_Jeu_Controller(this,RETOUR));



		centre_chargement_fichier = new JPanel();
		liste_fichier_chargement= Editeur_map.get_all_fichier();

		comboBox = new JComboBox();


		for (int i = 0; i < liste_fichier_chargement.length; i++) {
			comboBox.addItem(liste_fichier_chargement[i]);
		}

		comboBox.addActionListener((new Parametre_editor_Controller("Jcombobox",this)));
		centre_chargement_fichier.add(comboBox);



		this.add(solo);
		this.add(vs);
		this.add(coop);	
		this.add(bt_retour);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}

	public void refreshInfo() {
		repaint();
	}



}
