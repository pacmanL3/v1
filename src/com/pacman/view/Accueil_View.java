package com.pacman.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.pacman.controller.Accueil_Controller;
import com.pacman.controller.KeyListener_Controller;

public class Accueil_View extends Abs_Panel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String NEWGAME = "NEWGAME";
	public final static String EDITOR = "Editeur";
	public final static String OPTION = "Option";
	JButton bt_new_game ;
	JButton bt_editor ;
	JButton bt_option ;
	public Accueil_View(Abs_Fenetre _mother) {
		super(_mother);
		this.setMotherF(_mother);
	    this.setLayout(new GridLayout(8, 3));
	
	    bt_new_game =new JButton("Nouveau Jeu");
	    bt_new_game.setName(NEWGAME);

	    bt_editor =new JButton("Editeur");
	    bt_editor.setName(EDITOR);
	    
	    bt_new_game.addActionListener(new Accueil_Controller(this,NEWGAME));
	    bt_editor.addActionListener(new Accueil_Controller(this,EDITOR));
	    this.add(bt_new_game);
	    this.add(bt_editor);
	    
	    bt_option = new JButton("Option");
	    bt_option.setName(OPTION);
	    bt_option.addActionListener(new Accueil_Controller(this, OPTION));
	    this.add(bt_option);

	    
	    
	
	}

}
