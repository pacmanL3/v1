package com.pacman.view;

import com.pacman.tools.*;

import javax.swing.JFrame;

import com.pacman.controller.MouseMotionListener_controller;

public class Fenetre_Panneau_info_dev_View extends Abs_Fenetre{
	public Panneau_info_dev_View panneauInfo;
	public Fenetre_Panneau_info_dev_View(Abs_Fenetre fenetreView) {
		this.setMother(fenetreView);
		this.setTitle("info_dev");
        this.setDim(new Dimension(200, 200)); // une image fait 34*34 et je veux 12 * 12 
        this.setSize(getDim().getX(),getDim().getY());
        panneauInfo = new Panneau_info_dev_View(this);
		this.add(panneauInfo);
		this.setVisible(true);
	}
	@Override
	public void refreshInfo(){
		panneauInfo.refresh();
	}
}




