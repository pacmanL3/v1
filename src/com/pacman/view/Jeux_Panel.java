package com.pacman.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.pacman.controller.MouseMotionListener_controller;
import com.pacman.controller.MoveToClickListener;
import com.pacman.controller.MoveToMoveListener;
import com.pacman.tools.Dimension;

public class Jeux_Panel extends Abs_Panel {
	public Abs_Fenetre mother;
	ArrayList<Abs_Panel> panels;
	Abs_Panel head, left, right, center, foot, pause;
	boolean on_pause;

	public Jeux_Panel(Abs_Fenetre _mother, String game_mode, String controllsA, String controllsB) {
		super(_mother);
		mother = _mother;
		mother.setControlls(controllsA,controllsB);
		on_pause = false;
		setDim(new Dimension(this.getMotherF().getDim().getX(), this.getMotherF().getDim().getY()));
		mother.addMouseMotionListener(new MouseMotionListener_controller(mother));
		this.setLayout(null);
		
		panels = new ArrayList<Abs_Panel>();

		pause = new Pause_panel(this,getMotherF());
		head = new Head_Panel(this);
		center = new Center_Panel(this);

		panels.add(pause);
		panels.add(head);
		panels.add(center);

		switch (game_mode) {
		case Choix_Jeu_Panel.COOP: {
			foot = new Foot_Panel(this,getMotherF().getGame().getJoueur(1),getMotherF().getGame().getJoueur(2));
			panels.add(foot);
			right = new Joueur_Panel(this,getMotherF().getGame().getJoueur(2));
			panels.add(right);
			left = new Joueur_Panel(this,getMotherF().getGame().getJoueur(1));
			panels.add(left);
			break;
		}
		case Choix_Jeu_Panel.VS: {
			right = new Joueur_Panel(this,getMotherF().getGame().getJoueur(2));
			panels.add(right);
			left = new Joueur_Panel(this,getMotherF().getGame().getJoueur(1));
			panels.add(left);
			break;
		}
		case Choix_Jeu_Panel.SOLO: {
			left = new Joueur_Panel(this,getMotherF().getGame().getJoueur(1));
			panels.add(left);
			break;
		}
		default: {
			System.out.println("switch to default case");
			break;
		}
		}

		switch(mother.getControllsA()){
		case ChoixControls_Panel.CLICK_TO_GO:
			((Center_Panel)center).addMouseListener(new MoveToClickListener(((Center_Panel)center)));
			break;
		case ChoixControls_Panel.MOVE_TO_GO:
			((Center_Panel)center).addMouseMotionListener(new MoveToMoveListener(((Center_Panel)center)));
			break;
		case ChoixControls_Panel.KEY_MAP:
			((Joueur_Panel)left).buildKeyMap();
			break;
		case ChoixControls_Panel.KEYBOARD:
			break;
		default:System.out.println("controllsA non définis");
		}
		if(mother.getControllsB() != null){
			switch(mother.getControllsB()){
			case ChoixControls_Panel.CLICK_TO_GO:
				((Center_Panel)center).addMouseListener(new MoveToClickListener(((Center_Panel)center)));
				break;
			case ChoixControls_Panel.MOVE_TO_GO:
				((Center_Panel)center).addMouseMotionListener(new MoveToMoveListener(((Center_Panel)center)));
				break;
			case ChoixControls_Panel.KEY_MAP:
				((Joueur_Panel)right).buildKeyMap();
				break;
			case ChoixControls_Panel.KEYBOARD:
				break;
			default: System.out.println("controllsB non définis");
			}
		}
		pause.setVisible(on_pause);
		int hauteur,largeur,pos_x,pos_y;
		Abs_Panel e;
		for(int i = 0; i < panels.size(); i++){
			e = panels.get(i);
			hauteur = (int)(e.getRatioH() * this.getDim().getY());
			largeur = (int)(e.getRatioL() * this.getDim().getX());
			pos_x = (int)(e.getPOS_X() * this.getDim().getX());
			pos_y = (int)(e.getPOS_Y() * this.getDim().getY());
			e.setBounds(pos_x,pos_y,largeur,hauteur);
			e.setDim(new Dimension(largeur,hauteur));
			this.add(e);
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		pause.setVisible(on_pause);
		int hauteur,largeur,pos_x,pos_y;
		for(int i = 0; i < panels.size(); i++){
			Abs_Panel e = panels.get(i);
			hauteur = (int)(e.getRatioH() * this.getDim().getY());
			largeur = (int)(e.getRatioL() * this.getDim().getX());
			pos_x = (int)(e.getPOS_X() * this.getDim().getX());
			pos_y = (int)(e.getPOS_Y() * this.getDim().getY());
			e.setBounds(pos_x,pos_y,largeur,hauteur);
			this.add(e);
		}
	}

	@Override
	public void pause() throws InterruptedException {
		super.pause();
		((Fenetre_View)getMotherF()).pause();
		on_pause = true;
		repaint();
	}

	@Override
	public void resume() {
		super.resume();
		((Fenetre_View)getMotherF()).resume();
		on_pause = false;
		this.remove(pause);
		repaint();
	}

	@Override
	public void options() {
		super.options();
		this.getMotherF().options();
	}


	@Override
	public void refreshInfo() {
		super.refreshInfo();
		refreshDim();
		for(int i = 0; i < panels.size(); i++){
			panels.get(i).refreshInfo();
		}
		repaint();
	}

	public void refreshDim(){
		this.setDim(motherF.dim);
	}

	public Center_Panel getCenterPanel(){
		return (Center_Panel) this.center;
	}
}
