package com.pacman.tools;

import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.pacman.view.Abs_Fenetre;

public class GameTimer extends TimerTask{
	public static final long FRAMES_PER_SECOND = 100;
	private static final long TICK = 1000 /FRAMES_PER_SECOND;
	public static int loop = 0;

	Abs_Fenetre mother ;
	private boolean pause;



	public GameTimer(Abs_Fenetre _mother) {
		mother = _mother ;
		pause = false;
	}
	public static int getLoop() {
		return loop;
	}
	public static void resetlop() {
		GameTimer.loop = 0;
	}
	@Override
	public void run() {
		loop++;
		if(!pause){
			mother.refreshActions();
		}
	}

	public void start(){
		run();
		final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(this,0, TICK, TimeUnit.MILLISECONDS);
	}
	public boolean isPause() {
		return pause;
	}
	public void setPause(boolean pause) {
		this.pause = pause;
	}

}
