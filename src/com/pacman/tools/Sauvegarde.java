package com.pacman.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.pacman.model.Blinky;
import com.pacman.model.Bonus;
import com.pacman.model.Carte;
import com.pacman.model.Case;
import com.pacman.model.Couloir;
import com.pacman.model.Mur;
import com.pacman.model.PacMan;


/*
-- Structure du fichier de sauvegarde pour la MAP
0000 0000 
//nb de ligne , nb de colonne sur chacun 4 chiffres

//et le reste c'est la map

0 = couloir
1 = mur
2 = pos initial pacman1 
3 = pos initial pacman2
4 = pos initial des fantomes
5 = bonus 1
6 = bonus 2
7 = bonus 3
8 = bonus 4
9 = bonus invincible

*/



public class Sauvegarde {
	Carte savcarte;
	File f;
	int nbr_sav;
	public String nomdebase = "Sav";
	public String extension = ".map";
	FileWriter fichier;
	File fichierSav;

	public Sauvegarde(Carte carte) {
		savcarte = carte;
		nbr_sav = 0;
		fichierSav = null;
		System.out.println("traceur construteur sauvegarde");
	}


	public Sauvegarde() {
		nbr_sav = 0;
		fichierSav = null;
	}


	public void SaveMap(){
		sauvegardeString(convertMap());
		
	}
	
	public Carte LoadMap(String s){
		Carte res = ConvertStringtoCarte(s);
		return res;
	}
	
	private String convertMap(){ // converti la map en une une chaine de caractere
		String s ="";

		String ligne = ""+savcarte.getNb_lig();
		String colonne = ""+savcarte.getNb_col();
		
		for (int i = 0; i < 4- ligne.length(); i++) { // permet d'avoir le nombre de ligne sous la forme de 0005 pour 5 ligne pour cette exemple
			s+="0";
		}
		s += ligne;

		for (int i = 0; i < 4- colonne.length(); i++) { // mettre principe pour le nombre de colonne
			s+="0";
		}
		s += colonne;

		for (int i = 0; i < savcarte.getNb_lig(); i++) {
			for (int j = 0; j < savcarte.getNb_col(); j++) {
				s+= ConvertCasetoString(savcarte.get(i,j));
			}
		}
		return s;
	}
	

	public static int ConvertCasetoString(Case c) { // permet de convertir les case en int selon leur type ( je l'ai mis en static pour pouvoir l'utiliser dans le toString de carte
		switch (c.getType()) {
		case Case.COULOIR:
			return 0;
		case Case.MUR:
			return 1;
		case Case.PACMAN:
			return 2;			
		case Case.PACMAN2:
			return 3;			
		case Case.FANTOMES:
			return 4;			
		case Case.BONUS1:
			return 5;			
		case Case.BONUS2:
			return 6;			
		case Case.BONUS3:
			return 7;			
		case Case.BONUS4:
			return 8;	
		case Case.BONUS_GREAT:
			return 9;	
		default:
			break;
		}
		return -1;
	}

	public Carte ConvertStringtoCarte(String s) { // loading

		int nbrligne = extractnblignetoString(s);
		int nbrcolonne = extractnbColonnetoString(s);

		Carte carte_res = new Carte(nbrligne,nbrcolonne);

		System.out.println(s);
		int tmp = 8; // equivaut au debut de la map dans la string
		for (int ligne = 0; ligne < nbrligne; ligne++) {
			for (int colonne = 0; colonne < nbrcolonne; colonne++) {
				switch (s.charAt(tmp)) {
				case '0': // Couloir
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne));
					break;
				case '1': // mur
					carte_res.set(ligne,colonne, new Mur(ligne,colonne));
					break;
				case '2': // pacman
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne));
					//carte_res.set(ligne,colonne, new PacMan( ligne,colonne, Case.PACMAN));
					carte_res.SetPosinitialPacman1(colonne, ligne);
					break;
				case '3': // pacman2
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne));
					//carte_res.set(ligne,colonne, new PacMan( ligne,colonne, Case.PACMAN2));
					carte_res.SetPosinitialPacman2(colonne, ligne);
					break;
				case '4': // Fantomes
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne));
					//carte_res.set(ligne,colonne, new Blinky( ligne, colonne));
					carte_res.SetPosinitialfantomes(colonne, ligne);
					break;
				case '5': // Bonus 1
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne,new Bonus(Bonus.VAL0, false)));
					break;
				case '6': // Bonus 2
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne,new Bonus(Bonus.VAL1, false)));
					break;
				case '7': // Bonus 3
					System.out.println("pos x y :"+ligne+"/"+colonne );
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne,new Bonus(Bonus.VAL2, false)));
					break;
				case '8': // bonus 4
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne,new Bonus(Bonus.VAL3, false)));
					break;
				case '9': // bonus great
					carte_res.set(ligne,colonne, new Couloir(ligne,colonne,new Bonus(Bonus.VAL0, true)));
					break;
				default:
					System.out.println("traceur Sauvegarde convertstocarte defaut");
					break;
				}
				tmp ++;
			}
		}
		return carte_res;
	}
	
	
	
	public static int extractnblignetoString(String s){
		return Integer.parseInt(s.substring(0,4)); 
	}

	
	public static int extractnbColonnetoString(String s){
		return Integer.parseInt(s.substring(4,8)); 
		
	}
	

	
	


	public void sauvegardeString(String s) { // comprendre comment marche la sav 
		

	     File[] files = listeSauvegarde(); // permet de charger la liste de sauvegarde 
	     
	     if(files !=  null){
	    	 for (int i = 0; i < files.length; i++) {
				System.out.println("fichier");
				if (nbr_sav<=extractnumbersav(files[i].getName())) {
					nbr_sav = extractnumbersav(files[i].getName()) +1 ;
				}
			}
	     }
	     
	     if (fichierSav != null) {
	    	 System.out.println("traceur Sauvegarde fichier existant");
			nbr_sav = extractnumbersav(fichierSav.getName());
			DeleteFile(nbr_sav);
		}
	     
		final String chemin = "Sauvegarde/" + nomdebase + nbr_sav + extension;
		final File fichier = new File(chemin);
		try {
			// Creation du fichier
			fichier.createNewFile();
			// creation d'un writer 
			final FileWriter writer = new FileWriter(fichier);
			try {
				writer.write(s);
			} finally {
				// quoiqu'il arrive, on ferme le fichier
				writer.close();
			}
		} catch (Exception e) {
			System.out.println("Impossible de creer le fichier");
		}
	}
	
	public int extractnumbersav(String s){ // recupere le numero de sauvegarde d'un nom de fichier
		String tmp ="";
		int debut = 0 ;
		int fin = 0 ;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) =='v') {
				debut = i+1 ;
			}
			if (s.charAt(i) =='.') {
				fin = i ;
			}
		}
		System.out.println("hdfjilqshfjkdlsqbncvjklsdqbnfhjkl");
		return Integer.parseInt(s.substring(debut, fin));
	}
	
	
	public File[] listeSauvegarde(){
		File file = new File("Sauvegarde/");
	     File[] files = file.listFiles();
	     return files;
	}
	
	public File ChargefichierSav(int i){ 
		return listeSauvegarde()[i]; 
	}
	
	public Carte chargeMapfichier(String s) throws IOException{ // nom du fichier a charger et retourne la carte
		fichierSav = ChargefichierSav(extractnumbersav(s));
		return  LoadMap(ReadFile(ChargefichierSav(extractnumbersav(s)))) ;
		
	}
	public Carte chargeMapfichier() throws IOException{ // nom du fichier a charger et retourne la carte
		fichierSav = ChargefichierSav(extractnumbersav("Sav0.map"));
		return  LoadMap(ReadFile(fichierSav)) ;
		
	}
	
	
	
	public String ReadFile(File file) throws IOException{
		String res ="";
		try (BufferedReader br = new BufferedReader(new FileReader(file.getPath())))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				res+=sCurrentLine;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
		//System.out.println(res);

		return res;
	}


	public File ChargerDernierFichierSav() {
		return fichierSav;
	}

	
	public void setFichierSav(File fichierSav) {
		this.fichierSav = fichierSav;
	}

	public void EcraseSav() {
		
		
	}
	
	public void DeleteFile(int i){
		if(ChargefichierSav(i).exists()){
		File f = ChargefichierSav(i);
		System.out.println(f.getName());
		f.delete();
		}
		f =null;
	}
	
	public void LastDeleteFileLoaded(){
		f.delete();
	}

	
}