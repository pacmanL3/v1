package com.pacman.tools;

import java.awt.Rectangle;

public class Dimension {
	private int x;
	private int y;
	public Dimension(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public Dimension(Rectangle bounds) {
		this.x = bounds.width;
		this.y = bounds.height;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	@Override
	public String toString() {
		return "Dimension [x=" + x + ", y=" + y + "]";
	}
}
