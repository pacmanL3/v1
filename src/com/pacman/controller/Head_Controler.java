package com.pacman.controller;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pacman.view.Abs_Panel;
import com.pacman.view.Accueil_View;
import com.pacman.view.Fenetre_View;
import com.pacman.view.Head_Panel;
import com.pacman.view.Jeux_Panel;

public class Head_Controler extends Abs_ActionListener {
	
	public Head_Controler(Head_Panel _mother, String _name){
		this.setMother(_mother);
		this.setName(_name);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(((Component)arg0.getSource()).getName().equals(this.getName())){
			switch(this.getName()){
			case Head_Panel.HOME:
				//mettre en pause le jeu
				//prevenir le(s) joueur(s) que revenir à l'accueil quitte le jeu en cours
				//sauvegarder / quitter sans sauvegarder ?
				getMother().getMotherP().getMotherF().changerVue(new Accueil_View(getMother().getMotherP().getMotherF()));
				break;
			case Head_Panel.PAUSE:
				//mettre en pause le jeu
				//afficher le panel de pause par dessus le jeu
				try {
					((Jeux_Panel)getMother().getMotherP()).pause();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
							
				
				break;
			default:
				break;
			}
		}

	}

}
