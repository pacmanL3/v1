package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pacman.view.TestView;

/**
 * @author yrichi
 *
 *cette classe test nous permettra de tester rapidement des idées
 */
public class Test_controller implements ActionListener {
	String bt_nom;
	TestView view;
	public Test_controller(TestView _view,String _name) {
		bt_nom = _name ;
		view = _view ;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if ( ((Component) e.getSource()).getName() == bt_nom ){
			view.set_test("valide");
		}
		
	}

}
