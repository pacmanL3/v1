package com.pacman.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import com.pacman.model.Jeu;
import com.pacman.tools.Dimension;
import com.pacman.view.Center_Panel;
import com.pacman.view.ChoixControls_Panel;

public class MoveToClickListener implements MouseListener {

	private Center_Panel mother;
	private Jeu jeu;

	public MoveToClickListener(Center_Panel _mother){
		mother = _mother;
		jeu = mother.getGame();
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		Dimension pos = new Dimension(arg0.getX(),arg0.getY());
		Dimension caseToGo = jeu.convert_mouse_pos_to_case(pos);
		if(caseToGo.getX() >= 0 && caseToGo.getY() >= 0){
			mother.setCaseToGo(mother.getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.CLICK_TO_GO),caseToGo);
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

}
