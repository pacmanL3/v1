package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.pacman.model.Niveau;
import com.pacman.view.Abs_Panel;
import com.pacman.view.Accueil_View;
import com.pacman.view.ChoixControls_Panel;
import com.pacman.view.Choix_Jeu_Panel;
import com.pacman.view.Fenetre_View;
import com.pacman.view.Jeux_Panel;

public class Choix_Jeu_Controller extends Abs_ActionListener {

	public Choix_Jeu_Controller(Abs_Panel _mother, String _name) {
		setMother(_mother);
		setName(_name);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if(( ((Component)arg0.getSource()).getName().equals(this.getName()) )){
			if(this.getName().equals(Choix_Jeu_Panel.RETOUR)) {
				getMother().changerVue(new Accueil_View(getMother().getMotherF()));
			}else{
				getMother().changerVue(new ChoixControls_Panel(getMother().getMotherF(),this.getName(),"Sav0.map"));
			}
		}
	}
}
