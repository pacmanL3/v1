package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import com.pacman.view.Accueil_View;
import com.pacman.view.Head_Panel;
import com.pacman.view.Pause_panel;

public class Pause_Controller extends Abs_ActionListener{
	public Pause_Controller(Pause_panel _mother, String _name){
		this.setMother(_mother);
		this.setName(_name);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(((Component)arg0.getSource()).getName().equals(this.getName())){
			switch(this.getName()){
			case Pause_panel.HOME:
				//mettre en pause le jeu
				//prevenir le(s) joueur(s) que revenir à l'accueil quitte le jeu en cours
				//sauvegarder / quitter sans sauvegarder ?
				JOptionPane jop = new JOptionPane();			
				int option = jop.showConfirmDialog(null, "Voulez-vous sauvegarder avant de quitter le jeu ?", "Quitter le jeu", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
							
				if(option == JOptionPane.YES_OPTION){
					//sauvegarder avant de quitter
					//younes, à toi !!!
					getMother().getMotherF().changerVue(new Accueil_View(getMother().getMotherP().getMotherF()));
				}else if(option == JOptionPane.NO_OPTION){
					//ne pas sauvegarder avant de quitter
					getMother().getMotherF().changerVue(new Accueil_View(getMother().getMotherP().getMotherF()));
				}
				break;
			case Pause_panel.RESUME:
				//remettre en marche le jeu
				getMother().getMotherP().resume();
				break;
			case Pause_panel.OPTIONS:
				getMother().getMotherF().options();
			default:
				break;
			}
		}

	}
}
