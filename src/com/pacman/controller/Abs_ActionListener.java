package com.pacman.controller;

import java.awt.event.ActionListener;

import com.pacman.view.Abs_Panel;

public abstract class Abs_ActionListener implements ActionListener{
	private String name;
	private Abs_Panel mother;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Abs_Panel getMother() {
		return mother;
	}
	public void setMother(Abs_Panel mother) {
		this.mother = mother;
	}
	
	
}
