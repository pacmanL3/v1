package com.pacman.controller;

import java.awt.Component;
import java.awt.dnd.DragGestureListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.pacman.model.Carte;
import com.pacman.model.Case;
import com.pacman.model.Editeur_map;
import com.pacman.view.*;

public class Editeur_Controleur  extends Abs_ActionListener implements MouseListener, MouseWheelListener, MouseMotionListener, ActionListener {
	private Editeur_Panel mother;
	private String name = "-1";

	public Editeur_Controleur(Editeur_Panel _mother, String _name) {
		mother = _mother;
		name = _name;
		System.out.println("traceur controleur editeur");
	}

	public Editeur_Controleur(Editeur_Panel _mother) {
		mother = _mother;
	}

	public Editeur_Panel getMother() {
		return mother;
	}

	public void setMother(Editeur_Panel mother) {
		this.mother = mother;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		System.out.println("traceur Editeur_Controleur clique souris");
		Editeur_map jeuEditeur = mother.getMotherP().getMotherF().getEditeurJeu();
		Carte map_carte = jeuEditeur.getMap_game();
		int hauteur = mother.Hauteur_Panel_jeu();
		int largeur = mother.Largeur_Panel_jeu();
		int i = mother.getPos_x();
		int j = mother.getPos_y();
		int ligne = i/(largeur/(map_carte.getNb_col())); // pox_X(hauteur du panel jeu / le nombre de ligne )
		int colonne = j/(hauteur/(map_carte.getNb_lig()));
//		System.out.println("i : "+i+ " j: "+j+ "ligne "+ligne+" colonne "+colonne+" element selectionne "+jeu.getTmp().getType()+""
//				+ " taille fenetre Largeur"+mother.getSize().getWidth()+"\n\t"
//						+ " taille fenetre Largeur"+mother.getSize().getHeight()+"\n\t"
//								+ "Taille jeu Hauteur : "+ hauteur+"\n\t"
//										+ "Largeur : "+largeur+"\n\t"
//												+ "nbr ligne "+map_carte.getNb_lig()+"\n\t"
//												+ "nbr colonne"+map_carte.getNb_col()+"\n\t"
//												+"Detail calcul ligne "+ (float)((float)j/((float)hauteur/((float)map_carte.getNb_lig())))+"\n\t"
//														+"Detail calcul colonne "+ (float)((float)i/((float)largeur/((float)map_carte.getNb_col()))) );		
		
			jeuEditeur.getMap_game().set(colonne, ligne, jeuEditeur.getTmp());

		//System.out.println(jeuEditeur.getTmp().getType());
		//System.out.println(jeuEditeur.getMap_game().get(colonne,ligne));
		System.out.println(jeuEditeur.getMap_game().GetPosinitialfantomes());
		System.out.println(jeuEditeur.getMap_game().GetPosinitialPacman1());
		System.out.println(jeuEditeur.getMap_game().GetPosinitialPacman2());
		mother.repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		int i =  arg0.getX();
		int y =  arg0.getY();
//		System.out.println("test x "+i+"  y "+y);
		mother.setPos_x(i);
		mother.setPos_y(y);
		//System.out.println("test x "+mother.getPos_x()+"  y "+mother.getPos_y());
		mother.repaint();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		
		//System.out.println("test roulette"+arg0.getWheelRotation());
		
		if (arg0.getWheelRotation()<0) {
			System.out.println("precedent");
			mother.getMotherP().getMotherF().getEditeurJeu().previousSelect();
			System.out.println(mother.getMotherP().getMotherF().getEditeurJeu().getCurrentselect());
		}else{
			System.out.println("suivant");
			mother.getMotherP().getMotherF().getEditeurJeu().nextSelect();
			System.out.println(mother.getMotherP().getMotherF().getEditeurJeu().getCurrentselect());
		}
		mother.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals(name)) {	
		switch (name) {
		case Editeur_Panel.VALIDER_LABEL:
			mother.getMotherP().getMotherF().getEditeurJeu().sauvegarde_Map();
			System.out.println("carte valider");
			mother.getMotherP().getMotherF().changerVue(new Accueil_View(mother.getMotherP().getMotherF()));
			//System.out.println(mother.getMotherP().getMotherF().getEditeurJeu().getMap_game().toString());
			break;
		case Editeur_Panel.RETOUR_LABEL:
			System.out.println("traceur editeur bt retour");
			Parametre_editeur_Panel panel_edit = new Parametre_editeur_Panel(getMother().getMotherP()) ;
			if (mother.getMotherP().getMotherF().getEditeurJeu()!=null){ // permet de recuperer les parametres utilis�s lors de la creation de map
			panel_edit.setValeurColonne(mother.getMotherP().getMotherF().getEditeurJeu().getMap_game().getNb_col());
			panel_edit.setValeurligne(mother.getMotherP().getMotherF().getEditeurJeu().getMap_game().getNb_lig());
			}
			mother.getMotherP().getMotherF().changerVue(panel_edit);
			
			break;
		default:
			break;
		}
			 
			 
		}
	}
}
