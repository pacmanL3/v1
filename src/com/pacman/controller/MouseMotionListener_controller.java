package com.pacman.controller;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import com.pacman.view.Abs_Fenetre;
import com.pacman.view.Fenetre_View;
import com.pacman.view.Panneau_info_dev_View;

public class MouseMotionListener_controller implements MouseMotionListener {
	Abs_Fenetre mother;

	public MouseMotionListener_controller(Abs_Fenetre _mother) {
		// TODO Auto-generated constructor stub
		mother = _mother ;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {

		
		
		mother.alert_pos_changed(arg0.getX(),arg0.getY());
	}

}
