package com.pacman.controller;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import com.pacman.view.Abs_Fenetre;

public class Windows_resize_listener_Controller implements ComponentListener {
	public Abs_Fenetre mother;
	public Windows_resize_listener_Controller(Abs_Fenetre _mother) {
		// TODO Auto-generated constructor stub
		mother = _mother;
	}
	
	
	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentResized(ComponentEvent e) {
		mother.refreshInfo();
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

}
