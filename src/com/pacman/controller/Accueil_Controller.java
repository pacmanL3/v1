package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pacman.model.Niveau;
import com.pacman.view.Abs_Fenetre;
import com.pacman.view.Abs_Panel;
import com.pacman.view.Accueil_View;
import com.pacman.view.Choix_Jeu_Panel;
import com.pacman.view.Jeux_Panel;
import com.pacman.view.Parametre_editeur_Panel;

public class Accueil_Controller implements ActionListener {

	private Accueil_View mother;
	private String name;

	public Accueil_Controller(Accueil_View _mother, String _name) {
		this.mother = _mother;
		this.name = _name;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((Component) arg0.getSource()).getName().equals(name)) {
			switch (name) {
			case Accueil_View.NEWGAME:
				mother.changerVue(new Choix_Jeu_Panel(mother.getMotherF()));
				break;
			case Accueil_View.EDITOR:
				mother.changerVue(new Parametre_editeur_Panel(mother));
				break;
			case Accueil_View.OPTION:
				mother.getMotherF().options();
				break;
			default:
				break;
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
