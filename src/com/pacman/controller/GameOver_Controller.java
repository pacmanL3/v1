package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pacman.view.Accueil_View;
import com.pacman.view.Fenetre_View;
import com.pacman.view.GameOver_Panel;
import com.pacman.view.Jeux_Panel;
import com.pacman.view.Victory_Panel;

public class GameOver_Controller extends Abs_ActionListener{

	public GameOver_Controller(GameOver_Panel _mother, String _name){
		this.setMother(_mother);
		this.setName(_name);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(((Component)arg0.getSource()).getName().equals(this.getName())){
			switch(this.getName()){
			case GameOver_Panel.RECOMMENCER:
				getMother().getMotherF().newGame(((Fenetre_View)getMother().getMotherF()).getGame().getMode());
				getMother().getMotherF().changerVue(new Jeux_Panel(((Fenetre_View)getMother().getMotherF()), ((Fenetre_View)getMother().getMotherF()).getGame().getMode(),((Fenetre_View)getMother().getMotherF()).getControllsA(),((Fenetre_View)getMother().getMotherF()).getControllsB()));
				((Fenetre_View)getMother().getMotherF()).getTimer_game().setPause(false);
				((Fenetre_View)getMother().getMotherF()).getGame().init("Sav0.map");
				break;
			case GameOver_Panel.HOME:
				((GameOver_Panel)getMother()).getMotherF().changerVue(new Accueil_View(((GameOver_Panel)getMother()).getMotherF()));
				break;
			}
		}

	}

}
