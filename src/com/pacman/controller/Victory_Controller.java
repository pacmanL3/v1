package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pacman.view.Abs_Panel;
import com.pacman.view.Accueil_View;
import com.pacman.view.Fenetre_View;
import com.pacman.view.Jeux_Panel;
import com.pacman.view.Victory_Panel;

public class Victory_Controller extends Abs_ActionListener{

	public Victory_Controller(Abs_Panel _mother, String _name){
		this.setMother(_mother);
		this.setName(_name);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(((Component)arg0.getSource()).getName().equals(this.getName())){
			switch(this.getName()){
			case Victory_Panel.CONTINUER:
				getMother().getMotherF().changerVue(new Jeux_Panel(((Fenetre_View)getMother().getMotherF()), getMother().getMotherF().getGame().getMode(),((Fenetre_View)getMother().getMotherF()).getControllsA(),((Fenetre_View)getMother().getMotherF()).getControllsB()));
				getMother().getMotherF().getGame().nextLevel();
				break;
			case Victory_Panel.HOME:
				((Victory_Panel)getMother()).getMotherF().changerVue(new Accueil_View(((Victory_Panel)getMother()).getMotherF()));
				break;
			}
		}

	}
}