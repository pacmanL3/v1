package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pacman.view.Abs_Fenetre;
import com.pacman.view.Abs_Panel;
import com.pacman.view.ChoixControls_Panel;
import com.pacman.view.Choix_Jeu_Panel;
import com.pacman.view.Fenetre_View;
import com.pacman.view.GameOver_Panel;
import com.pacman.view.Jeux_Panel;

public class ChoixControls_Controller extends Abs_ActionListener{

	public ChoixControls_Controller(Abs_Panel _mother, String _name){
		this.setMother(_mother);
		this.setName(_name);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(((Component)arg0.getSource()).getName().equals(this.getName())){
			switch(this.getName()){
			case ChoixControls_Panel.OK:
				if(((ChoixControls_Panel)getMother()).getSelectionA() == null){
					//alert + retour
				}else{
					if(!((ChoixControls_Panel)getMother()).mode.equals(Choix_Jeu_Panel.SOLO)){
						if(((ChoixControls_Panel)getMother()).getSelectionB() == null){
							//alert + retour
						}else{
							((Fenetre_View)getMother().getMotherF()).newGame(((ChoixControls_Panel)getMother()).mode);
							getMother().changerVue(
										new Jeux_Panel(
												getMother().getMotherF(),
												((ChoixControls_Panel)getMother()).mode,
												((ChoixControls_Panel)getMother()).getSelectionA(),
												((ChoixControls_Panel)getMother()).getSelectionB()
											)
										);
							((Fenetre_View)getMother().getMotherF()).getGame().init(((ChoixControls_Panel)getMother()).getChoixCarte());
							((Fenetre_View)getMother().getMotherF()).newTimer();
						}
					}else{
						((Fenetre_View)getMother().getMotherF()).newGame(((ChoixControls_Panel)getMother()).mode);
						getMother().changerVue(
									new Jeux_Panel(
										getMother().getMotherF(),
										((ChoixControls_Panel)getMother()).mode,
										((ChoixControls_Panel)getMother()).getSelectionA(),
										((ChoixControls_Panel)getMother()).getSelectionB()
									)
								);
						((Fenetre_View)getMother().getMotherF()).getGame().init(((ChoixControls_Panel)getMother()).getChoixCarte());
						((Fenetre_View)getMother().getMotherF()).newTimer();
					}
				}
				break;
			case ChoixControls_Panel.RETOUR:
				getMother().getMotherF().changerVue(new Choix_Jeu_Panel(getMother().getMotherF()));
				break;
			default:break;
			}
		}

	}

}
