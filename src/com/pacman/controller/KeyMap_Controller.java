package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;

import com.pacman.view.Abs_Fenetre;
import com.pacman.view.ChoixControls_Panel;
import com.pacman.view.Joueur_Panel;


public class KeyMap_Controller extends Abs_ActionListener{

	public KeyMap_Controller(Joueur_Panel _mother, String _name){
		this.setMother(_mother);
		this.setName(_name);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(((Component)e.getSource()).getName().equals(this.getName())){
			int joueur = getMother().getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.KEY_MAP);
			switch(this.getName()){
			case Joueur_Panel.H:
				getMother().getMotherP().getMotherF().set_Last_key_pressed(Abs_Fenetre.HAUT,joueur);
				break;
			case Joueur_Panel.B:
				getMother().getMotherP().getMotherF().set_Last_key_pressed(Abs_Fenetre.BAS,joueur);
				break;
			case Joueur_Panel.G:
				getMother().getMotherP().getMotherF().set_Last_key_pressed(Abs_Fenetre.GAUCHE,joueur);
				break;
			case Joueur_Panel.D:
				getMother().getMotherP().getMotherF().set_Last_key_pressed(Abs_Fenetre.DROITE,joueur);
				break;
			default : System.out.println("bouton appuyé inexistant !");
			}
		}
	}

}
