package com.pacman.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.pacman.view.Abs_Fenetre;
import com.pacman.view.ChoixControls_Panel;
import com.pacman.view.Fenetre_View;

public class KeyListener_Controller implements KeyListener {
	Abs_Fenetre mother;
	
public KeyListener_Controller(Abs_Fenetre _mother) {
	mother = _mother ;
}
	@Override
	public void keyPressed(KeyEvent e) {
		String k;
		switch (e.getKeyCode())
		{
			case KeyEvent.VK_RIGHT:
				k = mother.DROITE;
				break;
			case KeyEvent.VK_LEFT:
				k = mother.GAUCHE;
				break;
			case KeyEvent.VK_UP:
				k = mother.HAUT;
				break;
			case KeyEvent.VK_DOWN:
				k = mother.BAS;
				break;
			default : k = mother.get_Last_key_pressed(mother.getNumJoueurControls(ChoixControls_Panel.KEYBOARD));
		}
		((Fenetre_View)mother).set_Last_key_pressed(k,mother.getNumJoueurControls(ChoixControls_Panel.KEYBOARD));
	}

	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
