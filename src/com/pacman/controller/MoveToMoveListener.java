package com.pacman.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.pacman.model.Jeu;
import com.pacman.tools.Dimension;
import com.pacman.view.Center_Panel;
import com.pacman.view.ChoixControls_Panel;

public class MoveToMoveListener implements MouseMotionListener {

	private Center_Panel mother;
	private Jeu jeu;
	
	public MoveToMoveListener(Center_Panel _mother) {
		mother = _mother;
		jeu = mother.getGame();
	}
	
	@Override
	public void mouseDragged(MouseEvent arg0) {
		Dimension pos = new Dimension(arg0.getX(),arg0.getY());
		Dimension caseToGo = jeu.convert_mouse_pos_to_case(pos);
		if(caseToGo.getX() >= 0 && caseToGo.getY() >= 0){
			mother.setCaseToGo(mother.getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.MOVE_TO_GO),caseToGo);
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		Dimension pos = new Dimension(arg0.getX(),arg0.getY());
		Dimension caseToGo = jeu.convert_mouse_pos_to_case(pos);
		if(caseToGo.getX() >= 0 && caseToGo.getY() >= 0){
			mother.setCaseToGo(mother.getMotherP().getMotherF().getNumJoueurControls(ChoixControls_Panel.MOVE_TO_GO),caseToGo);
		}
	}


}
