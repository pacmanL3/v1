package com.pacman.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import com.pacman.model.Carte;
import com.pacman.tools.Sauvegarde;
import com.pacman.view.Abs_Panel;
import com.pacman.view.Accueil_View;
import com.pacman.view.Editeur_Panel;
import com.pacman.view.Parametre_editeur_Panel;

public class Parametre_editor_Controller extends Abs_ActionListener {
	public Parametre_editor_Controller(String _name, Abs_Panel _mother) {
		setMother(_mother);
		setName(_name);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if (getName().equals("Jcombobox")) {
			JComboBox combo = (JComboBox)arg0.getSource();
			System.out.println((String)combo.getSelectedItem());
			((Parametre_editeur_Panel)getMother()).getBtnCharger().setEnabled(true);
			
			
			
		}
		else {
			
		Parametre_editeur_Panel m = (Parametre_editeur_Panel) (getMother());

		if (((Component) (arg0.getSource())).getName().equals(getName())) {

			switch (this.getName()) {
			case Parametre_editeur_Panel.MOINSC:
				if (m.getValeurColonne()>0) {
					m.setValeurColonne(m.getValeurColonne()-1);
				}
				break;
			case Parametre_editeur_Panel.PLUSC:
					m.setValeurColonne(m.getValeurColonne()+1);
				break;
			case Parametre_editeur_Panel.MOINSL:
				if (m.getValeurligne()>0) {
					m.setValeurligne(m.getValeurligne()-1);
				}

				break;
			case Parametre_editeur_Panel.PLUSL:
					m.setValeurligne(m.getValeurligne()+1);
				break;
			case Parametre_editeur_Panel.VALIDER:
				System.out.println("traceur parametre editeur controller : nbr ligne nbr:"+m.getValeurligne()+" colonne :"+m.getValeurColonne());
				getMother().getMotherF().New_editeur(m.getValeurligne(), m.getValeurColonne());
				getMother().getMotherF().changerVue(new Editeur_Panel(getMother()));

				break;
			case Parametre_editeur_Panel.RETOUR:
				getMother().getMotherF().changerVue(new Accueil_View(getMother().getMotherF()));

				break;
			case Parametre_editeur_Panel.CHARGER:
				
//				System.out.println((String)m.getComboBox().getSelectedItem());
				Carte c = getMother().getMotherF().getEditeurJeu().chargefichier((String)m.getComboBox().getSelectedItem());
				String tmp =(String)m.getComboBox().getSelectedItem();
				getMother().getMotherF().New_editeur(tmp,c,c.getNb_lig(),c.getNb_col());
				getMother().getMotherF().changerVue(new Editeur_Panel(getMother()));
				System.out.println("charger un fichier");
				break;

			default:
				break;
			}

			
			// System.out.println("valider");
			// System.out.println(m.getValeurligne());
			// System.out.println(m.getValeurColonne());
		}

	}
	}

}
