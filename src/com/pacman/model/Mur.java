package com.pacman.model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Mur extends Fixe{
	
	public Mur(int i, int j) {
		super(i, j);
		setType(MUR);
		ImageIcon iMur = new ImageIcon("images/mur.jpg");
		img = iMur.getImage();
	}



}
