package com.pacman.model;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.pacman.model.Mobile.direction;
import com.pacman.tools.Dimension;
import com.pacman.tools.GameTimer;
import com.pacman.view.Abs_Fenetre;
import com.pacman.view.Jeux_Panel;

public class PacMan extends Mobile {

	public static final int GREAT = (int)GameTimer.FRAMES_PER_SECOND * 12;

	private int is_great;
	private int num;

	private Dimension caseToGo;


	public PacMan(Jeu jeu, int case_i, int case_j, int _num){
		super(jeu,case_i,case_j);
		setType(PACMAN);
		this.num = _num;
		this.vie = 3;
		this.dir = direction.stop;
		this.iPersonnage = new ImageIcon[8];
		this.iPersonnage[0] = new ImageIcon("images/pacman_"+"haut"+".png");
		this.iPersonnage[1] = new ImageIcon("images/pacman_"+"bas"+".png");
		this.iPersonnage[2] = new ImageIcon("images/pacman_"+"gauche"+".png");
		this.iPersonnage[3] = new ImageIcon("images/pacman_"+"droite"+".png");
		this.iPersonnage[4] = new ImageIcon("images/pacman_great_"+"haut"+".png");
		this.iPersonnage[5] = new ImageIcon("images/pacman_great_"+"bas"+".png");
		this.iPersonnage[6] = new ImageIcon("images/pacman_great_"+"gauche"+".png");
		this.iPersonnage[7] = new ImageIcon("images/pacman_great_"+"droite"+".png");
		this.img = iPersonnage[3].getImage();
		this.setGreat(0);
	}


	public PacMan( int i, int j, String pacman) {
		super(i,j);
		vie = 3;
		dir = direction.stop;
		iPersonnage = new ImageIcon[8];
		if (pacman.equals(Case.PACMAN)) {
			setType(PACMAN);
			num = 1;
			iPersonnage[0] = new ImageIcon("images/pacman_"+"haut"+".png");
			iPersonnage[1] = new ImageIcon("images/pacman_"+"bas"+".png");
			iPersonnage[2] = new ImageIcon("images/pacman_"+"gauche"+".png");
			iPersonnage[3] = new ImageIcon("images/pacman_"+"droite"+".png");
			iPersonnage[4] = new ImageIcon("images/pacman_great_"+"haut"+".png");
			iPersonnage[5] = new ImageIcon("images/pacman_great_"+"bas"+".png");
			iPersonnage[6] = new ImageIcon("images/pacman_great_"+"gauche"+".png");
			iPersonnage[7] = new ImageIcon("images/pacman_great_"+"droite"+".png");
		}
		else{
			num = 2;
			setType(PACMAN2);
			iPersonnage[0] = new ImageIcon("images/pacman2_"+"haut"+".png");
			iPersonnage[1] = new ImageIcon("images/pacman2_"+"bas"+".png");
			iPersonnage[2] = new ImageIcon("images/pacman2_"+"gauche"+".png");
			iPersonnage[3] = new ImageIcon("images/pacman2_"+"droite"+".png");
			iPersonnage[4] = new ImageIcon("images/pacman2_great_"+"haut"+".png");
			iPersonnage[5] = new ImageIcon("images/pacman2_great_"+"bas"+".png");
			iPersonnage[6] = new ImageIcon("images/pacman2_great_"+"gauche"+".png");
			iPersonnage[7] = new ImageIcon("images/pacman2_great_"+"droite"+".png");
		}
		this.img = iPersonnage[3].getImage();
	}

	private void setGreat(int _g){
		is_great = _g;
	}

	private void upGreat(int _g){
		if(_g < 0 && is_great <= 0) is_great = 0;
		else is_great += _g;
	}

	public boolean is_great(){
		return is_great > 0;
	}

	public int getGreat(){
		return is_great;
	}

	@Override
	public boolean canMove(Dimension distance) {
		Dimension dim_pacman = new Dimension((int)(((Jeux_Panel)jeu.getMother().getCurrentPanel()).getCenterPanel().getRatioL()*jeu.getMother().getDim().getX()/jeu.getNiveau().getMap().getNb_col()),(int)(((Jeux_Panel)jeu.getMother().getCurrentPanel()).getCenterPanel().getRatioH()*jeu.getMother().getDim().getY()/jeu.getNiveau().getMap().getNb_lig()));
		Dimension pos_f = new Dimension(this.getPos().getX(),this.getPos().getY());
		Dimension pos_f_case_hg,pos_f_case_hd,pos_f_case_bg,pos_f_case_bd;
		if(dir == direction.haut || dir == direction.bas){
			if(dir == direction.haut){
				pos_f.setY(pos_f.getY()-(int)(distance.getY()*VITESSE));
			}else{
				pos_f.setY(pos_f.getY()+(int)(distance.getY()*VITESSE)/*+dim_pacman.getY()-1*/);
			}
		}else{
			if(dir == direction.gauche){
				pos_f.setX(pos_f.getX()-(int)(distance.getX()*VITESSE));
			}else{
				pos_f.setX(pos_f.getX()+(int)(distance.getX()*VITESSE)/*+dim_pacman.getX()-1*/);
			}
		}
		if(pos_f.getX() < 0 || pos_f.getY() < 0)
			return false;
		//point en haut a gauche
		pos_f_case_hg = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_hg) == null){
			return false;
		}
		//point en haut a droite
		pos_f.setX(pos_f.getX()+dim_pacman.getX()-1);
		pos_f_case_hd = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_hd) == null){
			return false;
		}
		//point en bas à droite
		pos_f.setY(pos_f.getY()+dim_pacman.getY()-1);
		pos_f_case_bd = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_bd) == null){
			return false;
		}
		//point en bas à gauche
		pos_f.setX(pos_f.getX()-dim_pacman.getX()+1);
		pos_f_case_bg = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_bg) == null){
			return false;
		}
		return !jeu.getMap().get(pos_f_case_hg).getType().equals(Case.MUR)
				&&
				!jeu.getMap().get(pos_f_case_hd).getType().equals(Case.MUR)
				&&
				!jeu.getMap().get(pos_f_case_bd).getType().equals(Case.MUR)
				&&
				!jeu.getMap().get(pos_f_case_bg).getType().equals(Case.MUR);
	}

	public void changeNextDirCTG(Dimension distance){
		//innondation vers caseToGo
		Carte tmp = new Carte(jeu.getMap().getNb_lig(),jeu.getMap().getNb_col());
		for(int i = 0; i < tmp.getNb_lig(); i++){
			for(int j = 0; j < tmp.getNb_col(); j++){
				if(jeu.getMap().get(i,j).getType().equals(Case.MUR)){
					tmp.set(i, j, new Mur(i,j));
				}else{
					tmp.set(i, j, new Indice(i,j,0));
				}
			}
		}
		if(!tmp.get(caseToGo).getType().equals(Case.MUR)){
			((Indice)tmp.get(caseToGo)).setNum(1);

			boolean trouve = false;
			int tour = 1;
			Dimension pos_pacman = jeu.convert_dim_pixel_to_case(getPos());
			while(!trouve){
				for(int i = 0; i < tmp.getNb_lig(); i++){
					for(int j = 0; j < tmp.getNb_col(); j++){
						if(!tmp.get(i,j).getType().equals(Case.MUR) && ((Indice)tmp.get(i,j)).getNum() == tour){
							if(pos_pacman.getX() == i && pos_pacman.getY() == j){
								trouve = true;
							}else{
								if(!tmp.get(i+1,j).getType().equals(Case.MUR)){
									((Indice)tmp.get(i+1, j)).setNum(Math.min(tour+1,((Indice)tmp.get(i+1, j)).getNum()==0?tour+2:((Indice)tmp.get(i+1, j)).getNum()));
								}
								if(!tmp.get(i-1,j).getType().equals(Case.MUR)){
									((Indice)tmp.get(i-1, j)).setNum(Math.min(tour+1,((Indice)tmp.get(i-1, j)).getNum()==0?tour+2:((Indice)tmp.get(i-1, j)).getNum()));
								}
								if(!tmp.get(i,j+1).getType().equals(Case.MUR)){
									((Indice)tmp.get(i, j+1)).setNum(Math.min(tour+1,((Indice)tmp.get(i, j+1)).getNum()==0?tour+2:((Indice)tmp.get(i, j+1)).getNum()));
								}
								if(!tmp.get(i,j-1).getType().equals(Case.MUR)){
									((Indice)tmp.get(i, j-1)).setNum(Math.min(tour+1,((Indice)tmp.get(i, j-1)).getNum()==0?tour+2:((Indice)tmp.get(i, j-1)).getNum()));
								}
							}
						}
						if(trouve)break;
					}
					if(trouve)break;
				}
				tour++;
			}
			((Indice)tmp.get(pos_pacman)).setNum(-1);
			int val_min = tour;
			direction temp_dir = getDir();
			if(getNextDir()!=getDir()){
				setDir(getNextDir());
				if(!canMove(distance)){
					setDir(temp_dir);
				}
			}else{
				if(!tmp.get(pos_pacman.getX()+1,pos_pacman.getY()).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_pacman.getX()+1,pos_pacman.getY())).getNum() < val_min && ((Indice)tmp.get(pos_pacman.getX()+1,pos_pacman.getY())).getNum() > 0){
					val_min = ((Indice)tmp.get(pos_pacman.getX()+1,pos_pacman.getY())).getNum();
					temp_dir = direction.droite;
				}
				if(!tmp.get(pos_pacman.getX()-1,pos_pacman.getY()).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_pacman.getX()-1,pos_pacman.getY())).getNum() < val_min && ((Indice)tmp.get(pos_pacman.getX()-1,pos_pacman.getY())).getNum() > 0){
					val_min = ((Indice)tmp.get(pos_pacman.getX()-1,pos_pacman.getY())).getNum();
					temp_dir = direction.gauche;
				}
				if(!tmp.get(pos_pacman.getX(),pos_pacman.getY()+1).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_pacman.getX(),pos_pacman.getY()+1)).getNum() < val_min && ((Indice)tmp.get(pos_pacman.getX(),pos_pacman.getY()+1)).getNum() > 0){
					val_min = ((Indice)tmp.get(pos_pacman.getX(),pos_pacman.getY()+1)).getNum();
					temp_dir = direction.bas;
				}
				if(!tmp.get(pos_pacman.getX(),pos_pacman.getY()-1).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_pacman.getX(),pos_pacman.getY()-1)).getNum() < val_min && ((Indice)tmp.get(pos_pacman.getX(),pos_pacman.getY()-1)).getNum() > 0){
					val_min = ((Indice)tmp.get(pos_pacman.getX(),pos_pacman.getY()-1)).getNum();
					temp_dir = direction.haut;
				}
			}
			setNextDir(temp_dir);
			temp_dir = getDir();
			if(getNextDir()!=getDir()){
				setDir(getNextDir());
				if(!canMove(distance)){
					setDir(temp_dir);
				}
			}
			if(canMove(distance)){
				if(dir == direction.haut){
					pos.setY(pos.getY()-(int)(distance.getY()*VITESSE));
				}else if(dir == direction.bas){
					pos.setY(pos.getY()+(int)(distance.getY()*VITESSE));
				}else if(dir == direction.gauche){
					pos.setX(pos.getX()-(int)(distance.getX()*VITESSE));
				}else if(dir == direction.droite){
					pos.setX(pos.getX()+(int)(distance.getX()*VITESSE));
				}
			}else{
				caseToGo = null;
			}	
		}

	}


	public void changeNextDir(Dimension distance){
		Dimension dim_pacman = new Dimension((int)(((Jeux_Panel)jeu.getMother().getCurrentPanel()).getCenterPanel().getRatioL()*jeu.getMother().getDim().getX()/jeu.getNiveau().getMap().getNb_col()),(int)(((Jeux_Panel)jeu.getMother().getCurrentPanel()).getCenterPanel().getRatioH()*jeu.getMother().getDim().getY()/jeu.getNiveau().getMap().getNb_lig()));
		Dimension pos_f = new Dimension(this.getPos().getX(),this.getPos().getY());
		Dimension pos_f_case_hg,pos_f_case_hd,pos_f_case_bg,pos_f_case_bd;
		if(nextDir == direction.haut || nextDir == direction.bas){
			if(nextDir == direction.haut){
				pos_f.setY(pos_f.getY()-(int)(distance.getY()*VITESSE));
			}else{
				pos_f.setY(pos_f.getY()+(int)(distance.getY()*VITESSE)+dim_pacman.getY()-1);
			}
		}else{
			if(nextDir == direction.gauche){
				pos_f.setX(pos_f.getX()-(int)(distance.getX()*VITESSE));
			}else{
				pos_f.setX(pos_f.getX()+(int)(distance.getX()*VITESSE)+dim_pacman.getX()-1);
			}
		}
		if(pos_f.getX() < 0 || pos_f.getY() < 0);
		else{
			pos_f_case_hg = jeu.convert_dim_pixel_to_case(pos_f);
			//point en haut a droite
			pos_f.setX(pos_f.getX()+dim_pacman.getX()-1);
			pos_f_case_hd = jeu.convert_dim_pixel_to_case(pos_f);
			//point en bas à droite
			pos_f.setY(pos_f.getY()+dim_pacman.getY()-1);
			pos_f_case_bd = jeu.convert_dim_pixel_to_case(pos_f);
			//point en bas à gauche
			pos_f.setX(pos_f.getX()-dim_pacman.getX()+1);
			pos_f_case_bg = jeu.convert_dim_pixel_to_case(pos_f);
			if(!(jeu.getMap().get(pos_f_case_hg) == null)
					&&
					!(jeu.getMap().get(pos_f_case_hd) == null)
					&&
					!(jeu.getMap().get(pos_f_case_bd) == null)
					&&
					!(jeu.getMap().get(pos_f_case_bg) == null)
					&&
					!jeu.getMap().get(pos_f_case_hg).getType().equals(Case.MUR)
					&&
					!jeu.getMap().get(pos_f_case_hd).getType().equals(Case.MUR)
					&&
					!jeu.getMap().get(pos_f_case_bd).getType().equals(Case.MUR)
					&&
					!jeu.getMap().get(pos_f_case_bg).getType().equals(Case.MUR)){
				setDir(nextDir);
			}
		}

	}

	public void avancer(Dimension distance){
		this.upGreat(-1);
		if(caseToGo != null){
			changeNextDirCTG(distance);
		}else{
			changeNextDir(distance);
		}
		if(canMove(distance)){
			if(dir == direction.haut){
				pos.setY(pos.getY()-(int)(distance.getY()*VITESSE));
			}else if(dir == direction.bas){
				pos.setY(pos.getY()+(int)(distance.getY()*VITESSE));
			}else if(dir == direction.gauche){
				pos.setX(pos.getX()-(int)(distance.getX()*VITESSE));
			}else if(dir == direction.droite){
				pos.setX(pos.getX()+(int)(distance.getX()*VITESSE));
			}
		}
	}
	@Override
	public void setDir(direction _dir){
		this.dir = _dir;
		int b = is_great()?4:0;
		if(dir == dir.haut){
			img = iPersonnage[0+b].getImage();
		}else if(dir == dir.bas){
			img = iPersonnage[1+b].getImage();
		}else if(dir == dir.gauche){
			img = iPersonnage[2+b].getImage();
		}else if(dir == dir.droite){
			img = iPersonnage[3+b].getImage();
		}
	}

	public void eatBonus() {
		//faire disparaitre le bonus
		Dimension pacman_case = jeu.convert_dim_pixel_to_case(getPos());
		if(((Couloir)jeu.getMap().get(pacman_case)).getBonus().is_great()){
			upGreat(GREAT);
		}
		((Couloir)jeu.getMap().get(pacman_case)).setBonus(null);
		System.out.println("eat bonus");
	}

	public void eat(Fantome f){
		f.die();
	}
	public void die(){
		vie--;
		if(vie>0){
			jeu.getMother().set_Last_key_pressed(null,num);
			dir = direction.stop;
			nextDir = direction.stop;
			setPos(jeu.convert_dim_case_to_pixel(jeu.getNiveau().getPos_init_pacman()));
			jeu.replaceFantome();
		}else{
			jeu.gameOver(this);
		}
	}
	
	public void setCaseToGo(Dimension _caseToGo) {
		caseToGo = _caseToGo;
	}
}
