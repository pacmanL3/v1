package com.pacman.model;

import javax.swing.ImageIcon;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import com.pacman.model.Mobile.direction;
import com.pacman.tools.Dimension;

public class Inky extends Fantome {
	//blinky est le fantome rouge
	//il vous traque en vous suivant partout à travers le labyrinthe.

	
	
	public Inky(Jeu jeu, int i, int j) {
		super(jeu, i, j);
		iPersonnage = new ImageIcon[4];
		iPersonnage[0] = new ImageIcon("images/inky_"+"haut"+".png");
		iPersonnage[1] = new ImageIcon("images/inky_"+"bas"+".png");
		iPersonnage[2] = new ImageIcon("images/inky_"+"gauche"+".png");
		iPersonnage[3] = new ImageIcon("images/inky_"+"droite"+".png");
		img = iPersonnage[3].getImage();
	}
	
	@Override
	public String getName(){
		return "Inky";
	}

	public void avancerCommeBlinky(Dimension distance) {
		Carte tmp = new Carte(jeu.getMap().getNb_lig(),jeu.getMap().getNb_col());
		for(int i = 0; i < tmp.getNb_lig(); i++){
			for(int j = 0; j < tmp.getNb_col(); j++){
				if(jeu.getMap().get(i,j).getType().equals(Case.MUR)){
					tmp.set(i, j, new Mur(i,j));
				}else{
					tmp.set(i, j, new Indice(i,j,0));
				}
			}
		}
		if(jeu.getPacman()!=null){
			((Indice)tmp.get(jeu.convert_dim_pixel_to_case(jeu.getPacman().getPos()).getX(),jeu.convert_dim_pixel_to_case(jeu.getPacman().getPos()).getY())).setNum(1);
		}
		if(jeu.getPacman_bis()!=null){
			((Indice)tmp.get(jeu.convert_dim_pixel_to_case(jeu.getPacman_bis().getPos()))).setNum(1);
		}
		boolean trouve = false;
		int tour = 1;
		Dimension pos_blinky = jeu.convert_dim_pixel_to_case(getPos());
		while(!trouve){
			for(int i = 0; i < tmp.getNb_lig(); i++){
				for(int j = 0; j < tmp.getNb_col(); j++){
					if(!tmp.get(i,j).getType().equals(Case.MUR) && ((Indice)tmp.get(i,j)).getNum() == tour){
						if(pos_blinky.getX() == i && pos_blinky.getY() == j){
							trouve = true;
						}else{
							if(!tmp.get(i+1,j).getType().equals(Case.MUR)){
								((Indice)tmp.get(i+1, j)).setNum(Math.min(tour+1,((Indice)tmp.get(i+1, j)).getNum()==0?tour+2:((Indice)tmp.get(i+1, j)).getNum()));
							}
							if(!tmp.get(i-1,j).getType().equals(Case.MUR)){
								((Indice)tmp.get(i-1, j)).setNum(Math.min(tour+1,((Indice)tmp.get(i-1, j)).getNum()==0?tour+2:((Indice)tmp.get(i-1, j)).getNum()));
							}
							if(!tmp.get(i,j+1).getType().equals(Case.MUR)){
								((Indice)tmp.get(i, j+1)).setNum(Math.min(tour+1,((Indice)tmp.get(i, j+1)).getNum()==0?tour+2:((Indice)tmp.get(i, j+1)).getNum()));
							}
							if(!tmp.get(i,j-1).getType().equals(Case.MUR)){
								((Indice)tmp.get(i, j-1)).setNum(Math.min(tour+1,((Indice)tmp.get(i, j-1)).getNum()==0?tour+2:((Indice)tmp.get(i, j-1)).getNum()));
							}
						}
					}
					if(trouve)break;
				}
				if(trouve)break;
			}
			tour++;
		}
		((Indice)tmp.get(pos_blinky)).setNum(-1);
		int val_min = tour;
		direction temp_dir = getDir();
		if(getNextDir()!=getDir()){
			setDir(getNextDir());
			if(!canMove(distance)){
				setDir(temp_dir);
			}
		}else{
			if(!tmp.get(pos_blinky.getX()+1,pos_blinky.getY()).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_blinky.getX()+1,pos_blinky.getY())).getNum() < val_min && ((Indice)tmp.get(pos_blinky.getX()+1,pos_blinky.getY())).getNum() > 0){
				val_min = ((Indice)tmp.get(pos_blinky.getX()+1,pos_blinky.getY())).getNum();
				temp_dir = direction.droite;
			}
			if(!tmp.get(pos_blinky.getX()-1,pos_blinky.getY()).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_blinky.getX()-1,pos_blinky.getY())).getNum() < val_min && ((Indice)tmp.get(pos_blinky.getX()-1,pos_blinky.getY())).getNum() > 0){
				val_min = ((Indice)tmp.get(pos_blinky.getX()-1,pos_blinky.getY())).getNum();
				temp_dir = direction.gauche;
			}
			if(!tmp.get(pos_blinky.getX(),pos_blinky.getY()+1).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_blinky.getX(),pos_blinky.getY()+1)).getNum() < val_min && ((Indice)tmp.get(pos_blinky.getX(),pos_blinky.getY()+1)).getNum() > 0){
				val_min = ((Indice)tmp.get(pos_blinky.getX(),pos_blinky.getY()+1)).getNum();
				temp_dir = direction.bas;
			}
			if(!tmp.get(pos_blinky.getX(),pos_blinky.getY()-1).getType().equals(Case.MUR) && ((Indice)tmp.get(pos_blinky.getX(),pos_blinky.getY()-1)).getNum() < val_min && ((Indice)tmp.get(pos_blinky.getX(),pos_blinky.getY()-1)).getNum() > 0){
				val_min = ((Indice)tmp.get(pos_blinky.getX(),pos_blinky.getY()-1)).getNum();
				temp_dir = direction.haut;
			}
		}
		setNextDir(temp_dir);
		temp_dir = getDir();
		if(getNextDir()!=getDir()){
			setDir(getNextDir());
			if(!canMove(distance)){
				setDir(temp_dir);
			}
		}
		if(canMove(distance)){
			if(dir == direction.haut){
				pos.setY(pos.getY()-(int)(distance.getY()*VITESSE));
			}else if(dir == direction.bas){
				pos.setY(pos.getY()+(int)(distance.getY()*VITESSE));
			}else if(dir == direction.gauche){
				pos.setX(pos.getX()-(int)(distance.getX()*VITESSE));
			}else if(dir == direction.droite){
				pos.setX(pos.getX()+(int)(distance.getX()*VITESSE));
			}
		}
	}

}
