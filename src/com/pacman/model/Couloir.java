package com.pacman.model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Couloir extends Fixe{

	private Bonus contenu;
	
	public Couloir(int i, int j) {
		super(i, j);
		setType(COULOIR);
		ImageIcon iCouloir = new ImageIcon("images/couloir.jpg");
		img = iCouloir.getImage();
	}
	
	public Couloir(int i, int j, Bonus _contenu){
		super(i, j);
		setType(COULOIR);
		ImageIcon iCouloir = new ImageIcon("images/couloir.jpg");
		img = iCouloir.getImage();
		contenu = _contenu;
	}
	
	public void setBonus(Bonus _contenu){
		contenu = _contenu;
	}
	
	public Bonus getBonus(){
		return this.contenu;
	}
	
	
	public Image getImage() {
		if (contenu==null) {
			return img;
		}
		else{
			return contenu.getImage();
		}
	}

}
