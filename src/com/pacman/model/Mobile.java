package com.pacman.model;

import java.util.Random;

import javax.swing.ImageIcon;

import com.pacman.model.Mobile.direction;
import com.pacman.tools.Dimension;
import com.pacman.view.Abs_Fenetre;
import com.pacman.view.Jeux_Panel;


public class Mobile extends Case{
	public static final float VITESSE = (float) (0.1); // ici vitesse va nous permettre d'avoir un mouvement a chaque frame en le multipliant a la largeur d'une case 
	
	
	protected ImageIcon[] iPersonnage;
	public static enum direction{haut,bas,gauche,droite,stop;};
	protected direction dir;
	protected direction nextDir;
	protected Dimension pos;
	protected int vie;
	protected Jeu jeu;
	protected Random rand;

	public Mobile(Jeu _jeu, int case_i, int case_j){
		super(case_i,case_j);
		rand = new Random();
		jeu = _jeu;
		pos = jeu.convert_dim_case_to_pixel(new Dimension(case_i,case_j));
	}

	public Mobile(int case_i, int case_j){		
		super(case_i,case_j);
		rand = new Random();
		
	}
	
	public void setNextDir(String s){
		if (s != null) {
			switch (s) {
			case Abs_Fenetre.HAUT:
				setNextDir(direction.haut);
				break;
			case Abs_Fenetre.BAS:
				setNextDir(direction.bas);
				break;
			case Abs_Fenetre.GAUCHE:
				setNextDir(direction.gauche);
				break;
			case Abs_Fenetre.DROITE:
				setNextDir(direction.droite);
				break;
			default:
				break;
			}
		}
	}
	
	public void setNextDir(direction _next) {
		this.nextDir = _next;
	}
	
	public direction getNextDir(){
		return this.nextDir;
	}

	public void setDir(direction _dir){
		this.dir = _dir;
		if(dir == direction.haut){
			img = iPersonnage[0].getImage();
		}else if(dir == direction.bas){
			img = iPersonnage[1].getImage();
		}else if(dir == direction.gauche){
			img = iPersonnage[2].getImage();
		}else if(dir == direction.droite){
			img = iPersonnage[3].getImage();
		}
	}
	public direction getDir(){
		return this.dir;
	}
	
	public boolean canMove(Dimension distance){
		Dimension dim = new Dimension((int)(((Jeux_Panel)jeu.getMother().getCurrentPanel()).getCenterPanel().getRatioL()*jeu.getMother().getDim().getX()/jeu.getNiveau().getMap().getNb_col()),(int)(((Jeux_Panel)jeu.getMother().getCurrentPanel()).getCenterPanel().getRatioH()*jeu.getMother().getDim().getY()/jeu.getNiveau().getMap().getNb_lig()));
		Dimension pos_f = new Dimension(this.getPos().getX(),this.getPos().getY());
		Dimension pos_f_case_hg,pos_f_case_hd,pos_f_case_bg,pos_f_case_bd;
		if(dir == direction.haut || dir == direction.bas){
			if(dir == direction.haut){
				pos_f.setY(pos_f.getY()-(int)(distance.getY()*VITESSE));
			}else{
				pos_f.setY(pos_f.getY()+(int)(distance.getY()*VITESSE)/*+dim_pacman.getY()-1*/);
			}
		}else{
			if(dir == direction.gauche){
				pos_f.setX(pos_f.getX()-(int)(distance.getX()*VITESSE));
			}else{
				pos_f.setX(pos_f.getX()+(int)(distance.getX()*VITESSE)/*+dim_pacman.getX()-1*/);
			}
		}
		if(pos_f.getX() < 0 || pos_f.getY() < 0)
			return false;
		//point en haut a gauche
		pos_f_case_hg = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_hg) == null){
			return false;
		}
		//point en haut a droite
		pos_f.setX(pos_f.getX()+dim.getX()-1);
		pos_f_case_hd = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_hd) == null){
			return false;
		}
		//point en bas à droite
		pos_f.setY(pos_f.getY()+dim.getY()-1);
		pos_f_case_bd = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_bd) == null){
			return false;
		}
		//point en bas à gauche
		pos_f.setX(pos_f.getX()-dim.getX()+1);
		pos_f_case_bg = jeu.convert_dim_pixel_to_case(pos_f);
		if(jeu.getMap().get(pos_f_case_bg) == null){
			return false;
		}
		return !jeu.getMap().get(pos_f_case_hg).getType().equals(Case.MUR)
				&&
				!jeu.getMap().get(pos_f_case_hd).getType().equals(Case.MUR)
				&&
				!jeu.getMap().get(pos_f_case_bd).getType().equals(Case.MUR)
				&&
				!jeu.getMap().get(pos_f_case_bg).getType().equals(Case.MUR);
	}
	
	public void dirAlea(){
		int r = (rand.nextInt(4));
		if(r == 0){
			dir = direction.haut;
		}else if(r == 1){
			dir = direction.bas;
		}else if(r == 2){
			dir = direction.gauche;
		}else{
			dir = direction.droite;
		}
	}

	public void avancer(Dimension distance){

	}
	public void avancer(){

	}
	public void setPos(Dimension _pos){
		this.pos = _pos;
	}
	public Dimension getPos(){
		return this.pos;
	}
	public void setVie(int _vie){
		this.vie = _vie;
	}
	public int getVie(){
		return this.vie;
	}

	public void setDir(String s) { // j'ai pas compris ta maniere de faire j'ai
		// fait une fonction temporaire pour avancer
		if (s != null) {
			switch (s) {
			case Abs_Fenetre.HAUT:
				setDir(dir.haut);
				break;
			case Abs_Fenetre.BAS:
				setDir(dir.bas);
				break;
			case Abs_Fenetre.GAUCHE:
				setDir(dir.gauche);
				break;
			case Abs_Fenetre.DROITE:
				setDir(dir.droite);
				break;
			default:
				break;
			}
		}
	}
}
