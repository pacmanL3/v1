package com.pacman.model;

import java.awt.Image;

public abstract class Case {
	
	// pfs decors
	public final static String MUR = "MUR";
	public final static String COULOIR = "COULOIR";
	public final static String INDICE = "INDICE";
	
	
	// pfs mobile
	public final static String PACMAN = "PACMAN";
	public final static String PACMAN2 = "PACMAN2";
	public static final String FANTOMES = "fantomes";
//	public final static String BLINKY = "BLINKY";
//	public final static String PINKY = "PINKY";
//	public final static String INKY = "INKY";
//	public final static String CLYDE = "CLYDE";
	public static final String BONUS1 = "BONUS1";
	public static final String BONUS2 = "BONUS2";
	public static final String BONUS3 = "BONUS3";
	public static final String BONUS4 = "BONUS4";
	public static final String BONUS_GREAT = "BONUS-GREAT";
	private int ligne;
	private int colonne;
	private static int hauteur;
	private static int largeur;
	private String type;
	Image img;

	public Case(int case_i, int case_j) {
		ligne = case_i;
		colonne = case_j;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}

	public int getHauteur() {
		return hauteur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public Image getImage() {
		return img;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
