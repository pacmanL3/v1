package com.pacman.model;

public class Indice extends Case {

	private int num;
	
	public Indice(int _i, int _j, int _num){
		super(_i, _j);
		setType(Case.INDICE);
		this.num = _num;
	}
	
	public void setNum(int _num){
		this.num = _num;
	}
	public int getNum(){
		return this.num;
	}
}
