package com.pacman.model;

import java.io.File;
import java.io.IOException;

import com.pacman.tools.Sauvegarde;

public class Editeur_map {
	Carte map_game;
	Sauvegarde sav;
	private int nbrligne;
	private int nbrcolonne;
	public final static String mur = Case.MUR;
	public final static String couloir = Case.COULOIR;
	public static final String posInitialPacman = Case.PACMAN;
	public static final String posInitialPacman2 = Case.PACMAN2;
	public static final String posInitialFantome = Case.FANTOMES;
	public static final String Bonus1 = Case.BONUS1;
	public static final String Bonus2 = Case.BONUS2;
	public static final String Bonus3 = Case.BONUS3;
	public static final String Bonus4 = Case.BONUS4;
	public static final String Bonus_great = Case.BONUS_GREAT;
	
	private String select[] = { mur, couloir,posInitialPacman,posInitialPacman2,posInitialFantome,Bonus1,Bonus2,Bonus3,Bonus4,Bonus_great };
	private int currentselect = 0;
	Jeu jeu ;
	private Case tmp; // coresponds a la selection de l'objet qui va suivre la
						// souris constamment

	public Editeur_map(int i, int j) {
		nbrligne = i;
		nbrcolonne = j;
		map_game = new Carte(nbrligne, nbrcolonne);
		for (int x = 0; x < map_game.getNb_lig(); x++) {
			for (int y = 0; y < map_game.getNb_col(); y++) {
				map_game.set(x, y, new Couloir(x, y));
			}

		}
		tmp = new Mur(0, 0);
		sav = new Sauvegarde(map_game);

	}

	public Editeur_map(String s,Carte c, int i, int j) {
		nbrligne = i;
		nbrcolonne = j;
		map_game = c;
		tmp = new Mur(0, 0);
		sav = new Sauvegarde(map_game);
		sav.setFichierSav(sav.ChargefichierSav(sav.extractnumbersav(s)));
		
	}

	public Case getCaseMapedit(int i, int j) {
		return map_game.get(i, j);
	}

	public String getSelect() {
		return select[currentselect];
	}

	public void setSelect(String[] select) {
		this.select = select;
	}

	public String getCurrentselect() {
		return select[currentselect];
	}

	public void nextSelect() { // passe a la selection suivante
		if (currentselect + 1 > select.length - 1) {
			currentselect = 0;
		} else {
			currentselect = currentselect + 1;
		}
		System.out.println(currentselect);
		refreshSelect();
	}

	private void refreshSelect() { // 
		switch (select[currentselect]) {
		case Case.MUR:
			tmp = new Mur(0, 0);
			break;
		case Case.COULOIR:
			tmp = new Couloir(0, 0);
			break;
		case Case.PACMAN:
			tmp = new PacMan( 0, 0,Case.PACMAN);
			//set init
		break;
		case Case.PACMAN2:
			tmp = new PacMan( 0, 0,Case.PACMAN2);
		break;
		
		case Case.FANTOMES:
			tmp = new Blinky(0, 0);
		break;
		
		case Case.BONUS1:
			tmp = new Couloir(0, 0,new Bonus(Bonus.VAL0, false));
		break;

		case Case.BONUS2:
			tmp = new Couloir(0, 0,new Bonus(Bonus.VAL1, false));
			break;

		case Case.BONUS3:
			tmp = new Couloir(0, 0,new Bonus(Bonus.VAL2, false));
			break;
		case Case.BONUS4:
			tmp = new Couloir(0, 0,new Bonus(Bonus.VAL3, false));
			break;
		case Case.BONUS_GREAT:
			tmp = new Couloir(0, 0,new Bonus(Bonus.VAL0, true));
			break;

		default:
			break;
		}
		
	}

	public void previousSelect() { // passe a la selection precedente
		if (currentselect == 0) {
			currentselect = select.length - 1;
		} else {
			currentselect = currentselect - 1;
		}
		refreshSelect();

	}

	public Case getTmp() {
		return tmp;
	}

	public void setTmp(Case tmp) {
		this.tmp = tmp;
	}

	public Carte getMap_game() {
		return map_game;
	}

	public void setMap_game(Carte map_game) {
		this.map_game = map_game;
	}

	public void sauvegarde_Map() {
			sav.SaveMap();
	}

	public static String[] get_all_fichier() { // recupere tt les fichiers
		Sauvegarde sav = new Sauvegarde();
		String[] res;
		if (sav.listeSauvegarde().length != 0) {
			File list[] = sav.listeSauvegarde();
			res = new String[list.length];
			for (int i = 0; i < list.length; i++) {
				res[i] = list[i].getName();

			}
		} else {
			res = new String[1];
			res[0] = "vide";
		}
		return res;
	}

	public static Carte chargefichier(String s) {
		Sauvegarde sav = new Sauvegarde();
		try {
			return sav.chargeMapfichier(s);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static Carte chargefichier() {
		Sauvegarde sav = new Sauvegarde();
		try {
			return sav.chargeMapfichier();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
