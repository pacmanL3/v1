package com.pacman.model;

import java.util.Random;

import javax.swing.ImageIcon;

import com.pacman.model.Mobile.direction;
import com.pacman.tools.Dimension;
import com.pacman.view.Jeux_Panel;

public class Fantome extends Mobile {

	public static final float VITESSE = (float) (0.1); // ici vitesse va nous permettre d'avoir un mouvement a chaque frame en le multipliant a la largeur d'une case 
	// exemple 0.1 * largeur d'une case = distance qui sera parcouru a chaque frame

	//Seul Blinky, le fantôme rouge, vous traque en vous suivant partout à travers le labyrinthe.
	//Pinky, le fantôme rose, cherchera toujours à se placer DEVANT vous pour vous tendre une embuscade
	//tout comme Inky, le fantôme bleu, d’ailleurs.
	//Quant à Clyde, le fantôme orange, son déplacement semble purement aléatoire
	//mais des petits malins qui ont disséqué le code source du jeu se sont rendus compte 
	//qu’il partait toujours vers le coin inférieur gauche du tableau quand il est trop près de Pac-Man.
	
	public Fantome(Jeu jeu,int case_i, int case_j) {
		super(jeu,case_i, case_j);
		vie = -1;
		dirAlea();
	}
	
	
	public Fantome(int ligne, int colonne) {
		super(ligne,colonne);
		vie = -1;
		dirAlea();
	}


	public String getName(){
		return "fantome";
	}
	
	@Override
	public void avancer(Dimension distance) {
		boolean avancer = false;
		while(!avancer){
			if(!canMove(distance))
				dirAlea();
			if(canMove(distance)){
				if(dir == direction.haut){
					pos.setY(pos.getY()-(int)(distance.getY()*VITESSE));
				}else if(dir == direction.bas){
					pos.setY(pos.getY()+(int)(distance.getY()*VITESSE));
				}else if(dir == direction.gauche){
					pos.setX(pos.getX()-(int)(distance.getX()*VITESSE));
				}else if(dir == direction.droite){
					pos.setX(pos.getX()+(int)(distance.getX()*VITESSE));
				}
				avancer = true;
			}
		}
	}
	public void eat(PacMan p){
		p.die();
	}
	public void die(){
		vie--;
		setPos(jeu.convert_dim_case_to_pixel(jeu.getNiveau().getPosHome()));
	}
}
