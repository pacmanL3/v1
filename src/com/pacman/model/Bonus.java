package com.pacman.model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Bonus {

	public static final int VAL0 = 1;
	public static final int VAL1 = 10;
	public static final int VAL2 = 100;
	public static final int VAL3 = 500;


	private int valeur;
	private boolean great;
	private Image img;

	public Bonus(int _valeur, boolean _great){
		this.valeur = _valeur;
		this.great = _great;
		ImageIcon iBonus;
		int val = 0;
		if(great){
			iBonus = new ImageIcon("images/Bonus0.png");
		}else{
			switch(valeur){
			case VAL0:
				val = 0;
			case VAL1 : 
				val = 1;
				break;
			case VAL2 : 
				val = 2;
				break;
			case VAL3 :
				val = 3;
				break;
			default :
				break;
			}
		}
		iBonus = new ImageIcon("images/Bonus"+val+".png");
		img = iBonus.getImage();
	}

	public int getValeur(){
		return this.valeur;
	}

	public boolean is_great(){
		return great;
	}
	public Image getImage(){
		return this.img;
	}
}
