package com.pacman.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import com.pacman.tools.Dimension;
import com.pacman.tools.Sauvegarde;

public class Carte_Bis_ {
	private Case[][] carte;
	private int nbr_ligne;
	private int nbr_colonne;
	
	Dimension[] posinitial; 
	/* position initial des elements mobiles , les indices nous permettent d'identifier  chaque type
			[0] = pos initial pacman1 
			[1] = pos initial pacman2
			[2] = pos initial f1 (inky)
			[3] = pos initial f2 (pinky)
			[4] = pos initial f3 ( clyde )
			[5] = pos initial f4 ( blinky )
	*/
	public Carte_Bis_(int _i, int _j) {
		Random rand = new Random();
		nbr_ligne = _j;
		nbr_colonne = _i;
		posinitial = new Dimension[7];
		carte = new Case[nbr_colonne][nbr_ligne];

		for (int i = 0; i < nbr_ligne; i++) {
			for (int j = 0; j < nbr_colonne; j++) {
				if (i == 0 || i == nbr_colonne - 1 || j == 0 || j == nbr_ligne - 1 || (i == nbr_ligne / 2)) {
					set(i,j,new Mur(i, j));
				} else {
					set(i,j,new Couloir(i, j,rand.nextInt(2)==0?null:new Bonus(Bonus.VAL3,false)));
				}
			}
		}
		
	}
	
	public Carte_Bis_(int[][] m, int _i, int _j){
		Random rand = new Random();
		nbr_ligne = _i;
		nbr_colonne = _j;
		posinitial = new Dimension[7];
		carte = new Case[nbr_colonne][nbr_ligne];
		

		String res = "";
		for (int i = 0; i < nbr_ligne; i++) {
			for (int j = 0; j < nbr_colonne; j++) {
				res += m[i][j]+" ";
			}
			System.out.println(res);
			res = "";
		}
		for (int i = 0; i < nbr_ligne; i++) {
			for (int j = 0; j < nbr_colonne; j++) {
				System.out.println("i = "+i+"\nj = "+j+"\nm["+i+"]["+j+"] = "+m[i][j]);
				/*if (m[i][j] == 1) {
					set(i,j,new Mur(i, j));
				} else {
					set(i,j,new Couloir(i, j,rand.nextInt(2)==0?null:new Bonus(Bonus.VAL3,false)));
				}*/
			}
		}
	}
	
	public Case get(int x, int y){
		if(x < 0 || y < 0 || y >= nbr_ligne || x >= nbr_colonne)
			return null;
		return carte[y][x];
	}
	
	public Case get(Dimension d){
		return get(d.getX(),d.getY());
	}
	
	public void set(int x, int y, Case c){
		if(x < 0 || y < 0 || y >= nbr_ligne || x >= nbr_colonne);
		else{
			carte[y][x] = c;
		}
	}
	
	public void set(Dimension d, Case c){
		set(d.getX(),d.getY(),c);
	}

	public void redimensionnementCase(int hauteur, int largeur) {
		for (int i = 0; i < nbr_colonne; i++) {
			for (int j = 0; j < nbr_ligne; j++) {
				carte[i][j].setHauteur(hauteur);
				carte[i][j].setLargeur(largeur);
			}
		}
	}

	public Case[][] getCarte() {
		return carte;
	}

	public void setCarte(Case[][] carte) {
		this.carte = carte;
	}

	public int getNbr_ligne() {
		return nbr_ligne;
	}

	public void setNbr_ligne(int nbr_ligne) {
		this.nbr_ligne = nbr_ligne;
	}

	public int getNbr_colonne() {
		return nbr_colonne;
	}

	public void setNbr_colonne(int nbr_colonne) {
		this.nbr_colonne = nbr_colonne;
	}
	
	public void setBonus(int x, int y, Bonus b){
		this.carte[y][x] = new Couloir(y, x, b);
	}

	@Override
	public String toString() {
		/*String res="";
		res += " [ nbr_ligne=" + nbr_ligne + ", nbr_colonne=" + nbr_colonne+ "]\n\n";
		for (int i = 0; i < carte.length; i++) {
			for (int j = 0; j < carte[0].length; j++) {
				res+=Sauvegarde.ConvertCasetoString(carte[i][j]);
			}
			res+='\n';
		}
			
		
				
		return res;*/
		String res = "";
		for(int i = 0; i < nbr_ligne; i++){
			for(int j = 0; j < nbr_colonne; j++){
				res += get(i,j)+"\t";
			}
			res+="\n";
		}
		return res;
	}

	public void SetPosinitialPacman1(int i ,int  j){
		posinitial[0] = new Dimension(i, j);
	}


	public void SetPosinitialPacman2(int i ,int  j){
		posinitial[1] = new Dimension(i, j);
	}
	

	public void SetPosinitialInky(int i ,int  j){
		posinitial[2] = new Dimension(i, j);
	}
	

	public void SetPosinitialPinky(int i ,int  j){
		posinitial[3] = new Dimension(i, j);
	}
	
	

	public void SetPosinitialClyde(int i ,int  j){
		posinitial[4] = new Dimension(i, j);
	}


	public void SetPosinitialBlinky(int i ,int  j){
		posinitial[5] = new Dimension(i, j);
	}

	
	public Dimension GetPosinitialPacman1(){
		return posinitial[0];
	}


	public Dimension GetPosinitialPacman2(){
		return posinitial[1];
	}
	

	public Dimension GetPosinitialInky(){
		return posinitial[2];
	}
	

	public Dimension GetPosinitialPinky(){
		return posinitial[3];
	}
	
	

	public Dimension GetPosinitialClyde(){
		return posinitial[4];
	}


	public Dimension GetPosinitialBlinky(){
		return posinitial[5];
	}
}



