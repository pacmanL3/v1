package com.pacman.model;

public class Joueur {

	private int num;
	
	private String name;
	private int score;
	private int max_level;
	private int lives;
	private int bonus;
	
	public Joueur(String _name, int _num){
		this.name = _name;
		this.score = 0;
		this.max_level = 1;
		this.lives = 3;
		this.bonus = 0;
		this.num = _num;
	}
	
	
	public Joueur(String _name, int _score, int _max_level, int _lives, int _bonus, int _num){
		this.name = _name;
		this.score = _score;
		this.max_level = _max_level;
		this.lives = _lives;
		this.bonus = _bonus;
		
		this.num = _num;
	}
	public int getNum(){
		return this.num;
	}
	public int getScore(){
		return this.score;
	}
	public void setScore(int _score){
		this.score = _score;
	}
	public void upScore(int _up){
		this.score += _up;
	}
	public void downScore(int _down){
		this.score -= _down;
	}
	public void upMaxLevel(){
		this.max_level++;
	}
	public int getMaxLevel(){
		return this.max_level;
	}
	public String getName(){
		return this.name;
	}
	public int getLives() {
		return lives;
	}
	public void upLive(){
		this.lives++;
	}
	public void downLive(){
		this.lives--;
	}
	public int getBonus() {
		return bonus;
	}
	public void upBonus(int _up){
		this.bonus += _up;
	}
	public void downBonus(int _down){
		this.bonus -= _down;
	}
	public void setBonus(int great) {
		this.bonus = great;
	}
}
