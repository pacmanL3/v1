package com.pacman.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.pacman.tools.Dimension;

public class Carte {

	private ArrayList<Case> carte;
	private int nb_col, nb_lig;

	private HashMap<String, Dimension> position_init;

	public Carte(int i, int j) {
		nb_lig = i;
		nb_col = j;
		carte = new ArrayList<>();
		for (int x = 0; x < i; x++) {
			for (int y = 0; y < j; y++) {
				carte.add(new Couloir(x, y));
			}
		}
		position_init = new HashMap<String, Dimension>();
	}

	public Carte(int[][] m, int i, int j) {
		position_init = new HashMap<String, Dimension>();
		nb_col = j;
		nb_lig = i;
		carte = new ArrayList<Case>();
		for (int x = 0; x < nb_lig; x++) {
			for (int y = 0; y < nb_col; y++) {
				switch (m[x][y]) {
				case 0:// couloir vide
					carte.add(new Couloir(i, j, null));
					break;
				case 1:// mur
					carte.add(new Mur(x, y));
					break;
				case 2:// pacman
					carte.add(new Couloir(i, j, null));
					position_init.put(Case.PACMAN, new Dimension(x, y));
					break;
				case 3:// pacman 2
					carte.add(new Couloir(i, j, null));
					position_init.put(Case.PACMAN2, new Dimension(x, y));
					break;
				case 4:// fantomes
					carte.add(new Couloir(i, j, null));
					position_init.put(Case.FANTOMES, new Dimension(x, y));
					break;
				case 5:// bonus point
					carte.add(new Couloir(i, j, new Bonus(Bonus.VAL0, false)));
					break;
				case 6:// bonus invincible
					carte.add(new Couloir(i, j, new Bonus(Bonus.VAL1, true)));
					break;
				default:
					System.out.println("default");
					break;
				}
			}
		}
	}


	public Case get(int i , int j){
		if(i >= nb_lig || j >=nb_col || i < 0 || j < 0)
			return null;
		return carte.get(j*nb_col+i);
	}

	public Case get(Dimension d) {
		return get(d.getX(), d.getY());

	}
	
	public void set(int i, int j, Case c){
		if(i >= nb_lig || j >=nb_col || i < 0 || j < 0);
		else carte.set(j*nb_lig+i, c);
	}

	public void set(Dimension d, Case c) {
		set(d.getX(), d.getY(), c);
	}

	public String toString() {
		String res = "";
		for (int i = 0; i < nb_col; i++) {
			for (int j = 0; j < nb_lig; j++) {
				res += get(i, j).getType() + "\t";
			}
			res += "\n";
		}
		return res;
	}
	
	public String toStringBis(){
		String res = "";
		for(int i = 0; i < nb_col; i++){
			for(int j = 0; j < nb_lig; j++){
				if(!get(i,j).getType().equals(Case.MUR))
					res += ((Indice)get(i,j)).getNum()+"\t";
				else
					res += "mur\t";
			}
			res += "\n";
		}
		return res;
	}

	public int getNb_col() {
		return nb_col;
	}

	public void setNb_col(int nb_col) {
		this.nb_col = nb_col;
	}

	public int getNb_lig() {
		return nb_lig;
	}

	public void setNb_lig(int nb_lig) {
		this.nb_lig = nb_lig;
	}

	public Dimension getInitPos(String s) {
		return position_init.get(s);
	}

	public void SetPosinitialPacman1(int i, int j) {
		position_init.put(Case.PACMAN, new Dimension(i, j));
	}

	public void SetPosinitialPacman2(int i, int j) {
		position_init.put(Case.PACMAN2, new Dimension(i, j));
	}

	public void SetPosinitialfantomes(int i, int j) {
		position_init.put(Case.FANTOMES, new Dimension(i, j));

	}
//
//	public void SetPosinitialPinky(int i, int j) {
//		position_init.put(Case.PINKY, new Dimension(i, j));
//	}
//
//	public void SetPosinitialClyde(int i, int j) {
//		position_init.put(Case.CLYDE, new Dimension(i, j));
//	}
//
//	public void SetPosinitialBlinky(int i, int j) {
//		position_init.put(Case.BLINKY, new Dimension(i, j));
//	}

	public Dimension GetPosinitialPacman1() {
		return position_init.get(Case.PACMAN);

	}

	public Dimension GetPosinitialPacman2() {
		return position_init.get(Case.PACMAN2);
	}

	public Dimension GetPosinitialfantomes() {
		return position_init.get(Case.FANTOMES);
	}

//	public Dimension GetPosinitialPinky() {
//		return position_init.get(Case.PINKY);
//	}
//
//	public Dimension GetPosinitialClyde() {
//		return position_init.get(Case.CLYDE);
//	}
//
//	public Dimension GetPosinitialBlinky() {
//		return position_init.get(Case.BLINKY);
//	}
}
