package com.pacman.model;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.pacman.model.Fantome;
import com.pacman.tools.Dimension;
import com.pacman.view.Accueil_View;
import com.pacman.view.Choix_Jeu_Panel;
import com.pacman.view.Fenetre_View;
import com.pacman.view.Jeux_Panel;
import com.pacman.view.Victory_Panel;

public class Jeu {

	private static final int max_level = 2;
	private int current_level;

	private Fenetre_View mother;
	private Niveau niveau;
	private PacMan pacman;
	private PacMan pacman_bis;
	private Fantome[] fantomes;
	private Joueur j1;
	private Joueur j2;
	private String mode;

	public Jeu(Fenetre_View _mother_view, String _mode){
		mother = _mother_view;
		this.mode = _mode;
		if(mode.equals(Choix_Jeu_Panel.SOLO)){
			j1 = new Joueur("Player 1",1);
			j2 = null;
		}else if(mode.equals(Choix_Jeu_Panel.VS) || mode.equals(Choix_Jeu_Panel.COOP)){
			j1 = new Joueur("Player 1",1);
			j2 = new Joueur("Player 2",2);
		}
	}

	public void init(String carte){
		niveau = new Niveau(j1,mode.equals(Choix_Jeu_Panel.SOLO)?j1:j2,carte);
		//niveau = new Niveau(j1,mode.equals(Choix_Jeu_Panel.SOLO)?j1:j2);
		pacman = new PacMan(this,niveau.getPos_init_pacman().getX(),niveau.getPos_init_pacman().getY(),1);
		if(!mode.equals(Choix_Jeu_Panel.SOLO)){
			pacman_bis = new PacMan(this,niveau.getPos_init_pacman().getX(),niveau.getPos_init_pacman().getY(),2);
		}else{
			pacman_bis = null;
		}
		fantomes = new Fantome[4];
		fantomes[0] = new Blinky(this,niveau.getPosHome().getX(),niveau.getPosHome().getY());
		fantomes[1] = new Inky(this,niveau.getPosHome().getX(),niveau.getPosHome().getY());
		fantomes[2] = new Pinky(this,niveau.getPosHome().getX(),niveau.getPosHome().getY());
		fantomes[3] = new Clyde(this,niveau.getPosHome().getX(),niveau.getPosHome().getY());
	}

	/*public void load(Joueur _j1, Joueur _j2, Niveau _niveau){
		j1.set
	}*/

	public Fenetre_View getMother() {
		return mother;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void totalGameOver(){
		mother.gameOver();
	}

	public PacMan getPacman() {
		return pacman;
	}
	public PacMan getPacman_bis(){
		return pacman_bis;
	}

	public Carte getMap() {
		return niveau.getMap();
	}

	public Fantome[] getFantomes() {
		return fantomes;
	}

	public boolean vs_Victory(){
		return mode.equals(Choix_Jeu_Panel.VS) && (pacman == null || pacman_bis == null);
	}

	public void avancer(){
		if(pacman == null && pacman_bis == null){
			totalGameOver();
		}else if(niveau.completed() || vs_Victory()){
			//changer la vue pour un ecran de victoire et recommencer ou non
			mother.changerVue(new Victory_Panel(mother,this));
		}else{
			int nbLignes = this.getMap().getNb_lig();
			int nbColones = this.getMap().getNb_col();
			int largeur = ((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getDim().getX()/nbColones;
			int hauteur = ((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getDim().getY()/nbLignes;
			Dimension distance = new Dimension(largeur,hauteur);
			if(pacman != null){
				j1.setBonus(pacman.getGreat());
				pacman.avancer(distance);
			}
			if(!mode.equals(Choix_Jeu_Panel.SOLO)){
				if(pacman_bis != null){
					j2.setBonus(pacman_bis.getGreat());
					pacman_bis.avancer(distance);
				}
			}
			for(int i = 0; i < fantomes.length; i++){
				switch(fantomes[i].getName()){
				case "Blinky":
					((Blinky)fantomes[i]).avancer(distance);
					break;
				case "Inky":
					//((Inky)fantomes[i]).avancer(distance);
					((Inky)fantomes[i]).avancerCommeBlinky(distance);
					break;
				case "Pinky":
					//((Pinky)fantomes[i]).avancer(distance);
					((Pinky)fantomes[i]).avancerCommeBlinky(distance);
					break;
				case "Clyde":
					//((Clyde)fantomes[i]).avancer(distance);
					((Clyde)fantomes[i]).avancerCommeBlinky(distance);
					break;
				default : break;
				}
			}
			eat();
		}
	}

	public void eat(){
		eat_pacman_fantome();
		if(pacman != null){
			Dimension pacman_case = convert_dim_pixel_to_case(pacman.getPos());
			if(((Couloir)(getMap().get(pacman_case))).getBonus()!= null){
				j1.upScore(((Couloir)(getMap().get(pacman_case))).getBonus().getValeur());
				if(((Couloir)(getMap().get(pacman_case))).getBonus().is_great()){
					j1.upBonus(PacMan.GREAT);
				}
				pacman.eatBonus();
			}
		}
		if(!mode.equals(Choix_Jeu_Panel.SOLO)){
			if(pacman_bis != null){
				Dimension pacman_bis_case = convert_dim_pixel_to_case(pacman_bis.getPos());
				if(((Couloir)(getMap().get(pacman_bis_case))).getBonus()!= null){
					j2.upScore(((Couloir)(getMap().get(pacman_bis_case))).getBonus().getValeur());
					if(((Couloir)(getMap().get(pacman_bis_case))).getBonus().is_great()){
						j2.upBonus(PacMan.GREAT);
					}
					pacman_bis.eatBonus();
				}
			}
		}
	}

	public void eat_pacman_fantome(){
		Dimension panel_dim = ((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getDim();
		Rectangle pacman_bounds = new Rectangle();
		if(pacman != null){
			pacman_bounds = new Rectangle(pacman.getPos().getX(),pacman.getPos().getY(),(int)(panel_dim.getX()/this.getMap().getNb_col()),(int)(panel_dim.getY()/this.getMap().getNb_lig()));
		}
		Rectangle pacman_bis_bounds = new Rectangle();
		if(!mode.equals(Choix_Jeu_Panel.SOLO) && pacman_bis != null){
			pacman_bis_bounds = new Rectangle(pacman_bis.getPos().getX(),pacman_bis.getPos().getY(),(int)(panel_dim.getX()/this.getMap().getNb_col()),(int)(panel_dim.getY()/this.getMap().getNb_lig()));
		}
		Rectangle temp_bounds;
		for(int i = 0; i < fantomes.length; i++){
			temp_bounds = new Rectangle(fantomes[i].getPos().getX(),fantomes[i].getPos().getY(),(int)(panel_dim.getX()/this.getMap().getNb_col()),(int)(panel_dim.getY()/this.getMap().getNb_lig()));
			if(pacman != null){
				if(colision(pacman_bounds,temp_bounds)){
					if(pacman.is_great()){
						j1.upScore(100);
						pacman.eat(fantomes[i]);
					}else{
						j1.downLive();
						fantomes[i].eat(pacman);
					}
				}
			}
			if(!mode.equals(Choix_Jeu_Panel.SOLO) && pacman_bis != null){
				if(colision(pacman_bis_bounds,temp_bounds)){
					if(pacman_bis.is_great()){
						j2.upScore(100);
						pacman_bis.eat(fantomes[i]);
					}else{
						j2.downLive();
						fantomes[i].eat(pacman_bis);
					}
				}	
			}
		}
	}

	public boolean colision(Rectangle r1, Rectangle r2){
		return r1.intersects(r2);
	}

	public Joueur getJoueur(int num){
		return num==1?j1:j2;
	}

	public String getMode(){
		return this.mode;
	}

	public Dimension convert_mouse_pos_to_case(Dimension pos){
		//-1 si en dehors du plateau de jeu 
		//calcul de l'offset
		Dimension relative_zero = new Dimension(((int)((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getPOS_X()*mother.getDim().getX()),((int)((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getPOS_Y()*mother.getDim().getY()));
		//maj de la position avec offset
		Dimension res = new Dimension(pos.getX()-relative_zero.getX(),pos.getY()-relative_zero.getY());
		return convert_dim_pixel_to_case(res);
	}

	public Dimension convert_dim_pixel_to_case(Dimension pos){
		//-1 si en dehors du plateau de jeu
		Dimension case_tmp = new Dimension((int)(((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getRatioL()*mother.getDim().getX()/niveau.getMap().getNb_col()),(int)(((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getRatioH()*mother.getDim().getY()/niveau.getMap().getNb_lig()));
		return new Dimension(pos.getX()/case_tmp.getX(),pos.getY()/case_tmp.getY());
	}
	public Dimension convert_dim_case_to_pixel(Dimension pos){
		Dimension temp_case = new Dimension((int)(((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getRatioL()*mother.getDim().getX()/niveau.getMap().getNb_col()),(int)(((Jeux_Panel)mother.getCurrentPanel()).getCenterPanel().getRatioH()*mother.getDim().getY()/niveau.getMap().getNb_lig()));
		return new Dimension(pos.getX()*temp_case.getX(),pos.getY()*temp_case.getY());
	}
	public void replaceFantome() {
		for(int i = 0; i < fantomes.length; i++){
			fantomes[i].setPos(convert_dim_case_to_pixel((getNiveau().getPosHome())));
		}
	}
	public void gameOver(PacMan p) {
		if(p.equals(pacman)){
			pacman = null;
		}else{
			pacman_bis = null;
		}
	}

	public int getCurrentLevel(){
		return this.current_level;
	}

	public int getNextLevel(){
		if(getCurrentLevel() == max_level){
			return -1;
		}else{
			return getCurrentLevel()+1;
		}
	}

	public int getMaxLevel(){
		return max_level;
	}

	public void nextLevel(){
		current_level ++;
		init("Sav0.map");
	}

	public void setCaseToGo(int num,Dimension caseToGo) {
		if(num == 1){
			if(pacman != null){
				pacman.setCaseToGo(caseToGo);
			}
		}else if(num == 2){
			if(pacman_bis != null){
				pacman_bis.setCaseToGo(caseToGo);
			}
		}
	}
}
