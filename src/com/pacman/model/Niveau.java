package com.pacman.model;

import java.util.ArrayList;

import com.pacman.tools.Dimension;
import com.pacman.view.Jeux_Panel;

public class Niveau {
	private int niveau_difficulte = 0 ;
	private String nom_niveau = "test_niv" ;
	private Carte map ;
	// je vais instancier en dur une map mais il faudrait une maniere d'enregistrer les données persistance a part ( bd sql ou simplement fichier texte )

	public Niveau(Joueur j1, Joueur j2, String carte){
		int max_level = Math.min(j1.getMaxLevel(), j2.getMaxLevel());
		map = Editeur_map.chargefichier();
		System.out.println(map.get(1,5).getType());
	}
	
	public Niveau(Joueur j1, Joueur j2){
		int max_level = Math.min(j1.getMaxLevel(), j2.getMaxLevel());
		ArrayList<int[][]> les_cartes = new ArrayList<int[][]>();
		int [][] temp = {
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
			{1,2,0,0,0,5,0,0,0,0,0,0,6,0,0,0,0,0,0,1},
			{1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,5,1,0,0,0,6,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,5,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,1},
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
			};
		les_cartes.add(temp);
		
		max_level = Math.min(max_level, les_cartes.size());
		
		map = new Carte(les_cartes.get(max_level-1),20,20);
	}
	
	public Niveau() {
		int[][] temp = 
			{	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,2,0,0,0,5,0,0,0,0,0,0,6,0,0,0,0,0,0,1},
				{1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,5,1,0,0,0,6,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,5,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
				{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
				};
		map = new Carte(temp,20,20);
		//System.out.println(map.toString());
		//map = new Carte(30, 30);
	}
	
	public int getNiveau_difficulte() {
		return niveau_difficulte;
	}
	public void setNiveau_difficulte(int niveau_difficulte) {
		this.niveau_difficulte = niveau_difficulte;
	}
	public String getNom_niveau() {
		return nom_niveau;
	}
	public void setNom_niveau(String nom_niveau) {
		this.nom_niveau = nom_niveau;
	}

	public Carte getMap() {
		return map;
	}

	public Dimension getPosHome() {
		return map.GetPosinitialfantomes();
	}

	public Dimension getPos_init_pacman() {
		return map.GetPosinitialPacman1();
	}

	public boolean completed() {
		int nb_bonus = 0;
		for(int i = 0; i < map.getNb_col(); i++){
			for(int j = 0; j < map.getNb_lig(); j++){
				if(map.get(i,j).getType().equals(Case.COULOIR)){
					nb_bonus += ((Couloir)map.get(i,j)).getBonus() == null ? 0 : 1;
				}
			}
		}
		return nb_bonus == 0;
	}
}
